
package com.samuel.dao;

import com.samuel.model.Empresa;
import java.util.List;

public interface EmpresaDao {
    
    public boolean crearEmpresa(Empresa em);
    public List<Empresa> listarEmpresa();
    public boolean actualizarEmpresa(Empresa em);
    public boolean eliminarEmpresa(int id);
    
   
}
