
package com.samuel.dao;

import com.samuel.model.Practica;
import java.util.List;

public interface PracticaDao {
    
    public boolean crearPractica(Practica pr);
    public List<Practica> listarPractica();
    public boolean actualizarPractica(Practica pr);
    public boolean eliminarPractica(int id);
    
}
