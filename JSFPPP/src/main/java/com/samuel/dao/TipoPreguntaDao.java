/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.dao;

import com.samuel.model.TipoPregunta;
import java.util.List;

/**
 *
 * @author Shamu
 */
public interface TipoPreguntaDao {
    
    public boolean crearTipoPregunta(TipoPregunta tipre);
    public List<TipoPregunta> listarTipoPregunta();
    public boolean actualizarTipoPregunta(TipoPregunta tipre);
    public boolean eliminarTipoPregunta(int id);
    
    
}
