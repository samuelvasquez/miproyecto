
package com.samuel.dao;

import com.samuel.model.Facultad;
import java.util.List;


public interface FacultadDao {
    public boolean crearFacultad(Facultad fa);
    public List<Facultad> listarFacultad();
    public boolean actualizarFacultad(Facultad fa);
    public boolean eliminarFacultad(int id);
}


