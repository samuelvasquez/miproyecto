/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.dao;

import com.samuel.model.Pregunta;
import java.util.List;

/**
 *
 * @author Shamu
 */
public interface PreguntaDao {
    
     public boolean crearPregunta(Pregunta pr);
    public List<Pregunta> listarPregunta();
    public boolean actualizarPregunta(Pregunta pr);
    public boolean eliminarPregunta(int id);
    
}
