
package com.samuel.dao;

import com.samuel.model.PersonaEp;
import java.util.List;

public interface PersonaEpDao {
    
    public boolean crearPersonaEp(PersonaEp pep);
    public List<PersonaEp> listarPersonaEp();
    public boolean actualizarPersonaEp(PersonaEp pep);
    public boolean eliminarPersonaEp(int id);
    
}
