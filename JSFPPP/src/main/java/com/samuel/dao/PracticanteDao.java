
package com.samuel.dao;

import com.samuel.model.Practicante;
import java.util.List;

public interface PracticanteDao {
    
     public boolean crearPracticante(Practicante pr);
    public List<Practicante> listarPracticante();
    public boolean actualizarPracticante(Practicante pr);
    public boolean eliminarPracticante(int id);
    
}
