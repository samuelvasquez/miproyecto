/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.dao;

import com.samuel.model.Alternativa;
import java.util.List;

/**
 *
 * @author Shamu
 */
public interface AlternativaDao {
    
    public boolean crearAlternativa(Alternativa al);
    public List<Alternativa> listarAlternativa();
    public boolean actualizarAlternativa(Alternativa al);
    public boolean eliminarAlternativa(int id);
    
    
}
