package com.samuel.dao;

import com.samuel.model.DocIdentidad;
import java.util.List;

public interface DocIdentidadDao {

    public boolean crearDocIdentidad(DocIdentidad di);

    public List<DocIdentidad> listarDocIdentidad();

    public boolean actualizarDocIdentidad(DocIdentidad di);

    public boolean eliminarDocIdentidad(int id);

}
