/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.dao;

import com.samuel.model.Cuestionario;
import java.util.List;

/**
 *
 * @author Shamu
 */
public interface CuestionarioDao {
    
        public boolean crearCuestionario(Cuestionario cus);
    public List<Cuestionario> listarCuestionario();
    public boolean actualizarCuestionario(Cuestionario cus);
    public boolean eliminarCuestionario(int id);
    
    
}
