/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.dao;

import com.samuel.model.TipoEvaluacion;
import java.util.List;

/**
 *
 * @author Shamu
 */
public interface TipoEvaluacionDao {
    
      public boolean crearTipoEvaluacion(TipoEvaluacion tev);
    public List<TipoEvaluacion> listarTipoEvaluacion();
    public boolean actualizarTipoEvaluacion(TipoEvaluacion tev);
    public boolean eliminarTipoEvaluacion(int id);
    
}
