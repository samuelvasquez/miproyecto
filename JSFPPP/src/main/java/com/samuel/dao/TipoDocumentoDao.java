package com.samuel.dao;

import com.samuel.model.TipoDocumento;
import java.util.List;

public interface TipoDocumentoDao {

    public boolean crearTipoDocumento(TipoDocumento tp);

    public List<TipoDocumento> listarTipoDocumento();

    public boolean actualizarTipoDocumento(TipoDocumento tp);

    public boolean eliminarTipoDocumento(int id);

}
