
package com.samuel.dao;
    
import com.samuel.model.RolOpcion;
import java.util.List;
    
public interface RolOpcionDao {
    
    public int crearMenu(RolOpcion rolp);
    public List<RolOpcion> listarRolOpcion();
    public boolean actualizarRolOpcion(RolOpcion rolp);
    public boolean eliminarRolOpcion(String id);
    
}
