package com.samuel.dao;

import com.samuel.model.Ciclo;
import java.util.List;

public interface CicloDao {

    public boolean crearCiclo(Ciclo ci);

    public List<Ciclo> listarCiclo();

    public boolean actualizarCiclo(Ciclo ci);

    public boolean eliminarCiclo(int id);

}
