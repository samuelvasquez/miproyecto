
package com.samuel.dao;

    import com.samuel.model.PersonaFacultad;
    import java.util.List;

public interface PersonaFacultadDao {
    
     public boolean crearPersonaFacultad(PersonaFacultad pf);
    public List<PersonaFacultad> listarPersonaFacultad();
    public boolean actualizarPersonaFacultad(PersonaFacultad pf);
    public boolean eliminarPersonaFacultad(int id);
    
}
