
package com.samuel.dao;

import com.samuel.model.Usuario;
import java.util.List;

public interface UsuarioDao {
    
    public boolean crearUsuario(Usuario us);
    public List<Usuario> listarUsuario();
    public boolean actualizarUsuario(Usuario us);
    public boolean eliminarUsuario(int id);
}
