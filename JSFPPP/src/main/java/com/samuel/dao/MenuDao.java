
package com.samuel.dao;

import com.samuel.model.Menu;
import java.util.List;

public interface MenuDao {
    public int crearMenu(Menu me);
    public List<Menu> listarMenu();
    public boolean actualizarMenu(Menu us);
    public boolean eliminarMenu(int id);
}
