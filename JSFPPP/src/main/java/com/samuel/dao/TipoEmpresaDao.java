package com.samuel.dao;

import com.samuel.model.TipoEmpresa;
import java.util.List;

public interface TipoEmpresaDao {

    public boolean crearTipoEmpresa(TipoEmpresa te);

    public List<TipoEmpresa> listarTipoEmpresa();

    public boolean actualizarTipoEmpresa(TipoEmpresa te);

    public boolean eliminarTipoEmpresa(int id);

}
