/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.dao;

import com.samuel.model.Evaluacion;
import java.util.List;

/**
 *
 * @author Shamu
 */
public interface EvaluacionDao {
    public boolean crearEvaluacion(Evaluacion ev);
    public List<Evaluacion> listarEvaluacion();
    public boolean actualizarEvaluacion(Evaluacion ev);
    public boolean eliminarEvaluacion(int id);
    
}
