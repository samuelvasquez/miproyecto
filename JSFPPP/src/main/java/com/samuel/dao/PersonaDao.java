
package com.samuel.dao;

import com.samuel.model.Persona;
import java.util.List;

public interface PersonaDao {
    
    public boolean crearPersona(Persona pe);
    public List<Persona> listarPersona();
    public boolean actualizarPersona(Persona pe);
    public boolean eliminarPersona(int id);
    
    
    
}
