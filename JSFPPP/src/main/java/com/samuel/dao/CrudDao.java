
package com.samuel.dao;


import com.samuel.model.Crud;
import java.util.List;

public interface CrudDao {
     public int CrearCrud(Crud cr);
     public List<CrudDao>ListarCrud();
     public boolean actualizarCrud(Crud cr);
     public boolean eliminarCrud(int id);
     
    
}
