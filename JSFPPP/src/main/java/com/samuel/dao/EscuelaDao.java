
package com.samuel.dao;

import com.samuel.model.Escuela;
import java.util.List;

public interface EscuelaDao {
    
    public boolean crearEscuela(Escuela es);
    public List<Escuela> listarEscuela();
    public boolean actualizarEscuela(Escuela es);
    public boolean eliminarEscuela(int id);
    
}
