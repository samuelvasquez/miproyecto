
package com.samuel.dao;

    import com.samuel.model.Rol;
    import java.util.List;

public interface RolDao {
    
     public boolean crearRol(Rol rol);
    public List<Rol> listarRol();
    public boolean actualizarRol(Rol rol);
    public boolean eliminarRol(int id);
    
}
