
package com.samuel.dao;
    
    import com.samuel.model.SubMenu;
    import java.util.List;

public interface SubMenuDao {
    
    public int crearMenu(SubMenu sm);
    public List<SubMenu> listarSubMenu();
    public boolean actualizarSubMenu(SubMenu sm);
    public boolean eliminarSubMenu(String id);
    
}
