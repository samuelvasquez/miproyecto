
package com.samuel.dao;

    import com.samuel.model.Evidencia;
    import java.util.List;

public interface EvidenciaDao {
    
    public boolean crearEvidencia(Evidencia ev);
    public List<Evidencia> listarEvidencia();
    public boolean actualizarEvidencia(Evidencia ev);
    public boolean eliminarEvidencia(String id);
    
    
}
