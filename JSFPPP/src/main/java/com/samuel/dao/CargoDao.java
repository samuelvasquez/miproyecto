
package com.samuel.dao;

import com.samuel.model.Cargo;
import java.util.List;


public interface CargoDao {
    
public boolean crearCargo(Cargo ca);
    public List<Cargo> listarCargo();
    public boolean actualizarCargo(Cargo ca);
    public boolean eliminarCargo(int id);
    
    
    
}
