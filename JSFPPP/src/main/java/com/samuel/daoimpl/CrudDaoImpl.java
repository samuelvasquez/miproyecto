
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.CrudDao;
import com.samuel.model.Crud;
import java.util.List;

public class CrudDaoImpl implements CrudDao {

    Conexionbd cx;
    String sql;
    
     public CrudDaoImpl() {
        cx = new Conexionbd();
    }

    
    @Override
    public int CrearCrud(Crud cr) {
        sql = "INSERT INTO public.crud(nombre) "
                + "VALUES('"+cr.getNombre()+"')";
        cx.performKeys(sql);
        return cx.performKeys(sql);
    }

    @Override
    public List<CrudDao> ListarCrud() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizarCrud(Crud cr) {
        sql="UPDATE public.crud SET"
                +" nombre='"+cr.getNombre()+"' "
                + " WHERE id_crud="+cr.getId_crud()+"";   
         cx.executeInsertUpdate(sql); 
         
         return false;
    }

    @Override
    public boolean eliminarCrud(int  id) {
        sql="DELETE FROM public.crud WHERE id_crud="+id; 
        return cx.executeInsertUpdate(sql);  
    }
    
}
