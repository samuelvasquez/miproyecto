package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.EmpresaDao;
import com.samuel.model.Empresa;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EmpresaDaoImpl implements EmpresaDao {

    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Empresa> listEmpresa;

    public EmpresaDaoImpl() {
        cx = new Conexionbd();
    }

    @Override
    public boolean crearEmpresa(Empresa em) {
        sql = "INSERT INTO public.empresa(id_tipo_empresa, nombre, ruc, estado) "
                + "VALUES (" + em.getId_tipo_empresa() + ",'" + em.getNombre() + "', '" + em.getRuc() + "', '" + em.getEstado() + "')";
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<Empresa> listarEmpresa() {
        listEmpresa = new ArrayList<>();

        sql = "SELECT * FROM public.empresa";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {

                listEmpresa.add(new Empresa(Integer.parseInt(rs.getString("id_empresa")), Integer.parseInt(rs.getString("id_tipo_empresa")),
                        rs.getString("nombre"), rs.getString("ruc"), "", rs.getString("estado")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(PersonaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listEmpresa;
    }

    @Override
    public boolean actualizarEmpresa(Empresa em) {
        sql = "UPDATE public.empresa SET"
                + " id_tipo_empresa=" + em.getId_tipo_empresa() + ", nombre='" + em.getNombre() + "', ruc='" + em.getRuc() + "', estado='" + em.getEstado() + "' "
                + " WHERE id_empresa=" + em.getId_empresa();
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public boolean eliminarEmpresa(int id) {
        sql = "DELETE FROM public.empresa WHERE id_empresa=" + id;
        return cx.executeInsertUpdate(sql);
    }

}
