/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.AlternativaDao;
import com.samuel.model.Alternativa;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Shamu
 */
public class AlternativaDaoImpl implements AlternativaDao{
    
    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Alternativa> listAlternativa;

    public AlternativaDaoImpl() {
        
         cx = new Conexionbd();
    }
    
    
    
    

    @Override
    public boolean crearAlternativa(Alternativa al) {
     sql = "INSERT INTO public.alternativa(nombre, id_pregunta) "
                + "VALUES('" + al.getNombre() + "', " + al.getId_pregunta()+")";
       return cx.executeInsertUpdate(sql); 
    
    }

    @Override
    public List<Alternativa> listarAlternativa() {
     listAlternativa = new ArrayList<>();

        sql = "SELECT * FROM public.Alternativa";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {

                listAlternativa.add(new Alternativa(Integer.parseInt(rs.getString("id_alternativa")), rs.getString("nombre"), Integer.parseInt(rs.getString("id_pregunta"))));
            }

        } catch (SQLException ex) {
            Logger.getLogger(AlternativaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listAlternativa;
    }

    @Override
    public boolean actualizarAlternativa(Alternativa al) {
         sql = "UPDATE public.alternativa SET"
                + " nombre='" + al.getNombre() + "', id_pregunta=" + al.getId_pregunta() + " "
                + " WHERE id_alternativa=" + al.getId_alternativa()+ "";
        cx.executeInsertUpdate(sql);

        return false;
    }

    @Override
    public boolean eliminarAlternativa(int id) {
        sql = "DELETE FROM public.alternativa WHERE id_alternativa= "+ id;
        return cx.executeInsertUpdate(sql);
    }
    
}
