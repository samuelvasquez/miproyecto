package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.EscuelaDao;
import com.samuel.model.Escuela;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EscuelaDaoImpl implements EscuelaDao {

    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Escuela> listEscuela;

    public EscuelaDaoImpl() {
        cx = new Conexionbd();
    }

    @Override
    public boolean crearEscuela(Escuela es) {
        sql = "INSERT INTO public.escuela_profesional(id_facultad, nombre, abreviatura, estado) "
                + "VALUES (" + es.getId_facultad() + ",'" + es.getNombre() + "', '" + es.getAbreviatura() + "', '" + es.getEstado() + "')";
        
        System.out.println("sql:"+sql);
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<Escuela> listarEscuela() {
        listEscuela = new ArrayList<>();

        sql = "SELECT * FROM public.escuela_profesional";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {
                listEscuela.add(new Escuela(Integer.parseInt(rs.getString("id_escuela_profesional")), Integer.parseInt(rs.getString("id_facultad")),
                        rs.getString("nombre"), rs.getString("abreviatura"), rs.getString("estado")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(EscuelaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listEscuela;
    }

    @Override
    public boolean actualizarEscuela(Escuela es) {
        sql = "UPDATE public.escuela_profesional SET"
                + " id_facultad=" + es.getId_facultad() + ", nombre='" + es.getNombre() + "', abreviatura='" + es.getAbreviatura() + "', estado='" + es.getEstado() + "' "
                + " WHERE id_escuela_profesional=" + es.getId_escuela();
        System.out.println("sql:"+sql);
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public boolean eliminarEscuela(int id) {
        sql = "DELETE FROM public.escuela_profesional WHERE id_escuela_profesional=" + id;
        return cx.executeInsertUpdate(sql);
    }

}
