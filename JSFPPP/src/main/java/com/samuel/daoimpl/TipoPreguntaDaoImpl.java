/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.TipoPreguntaDao;
import com.samuel.model.TipoPregunta;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Shamu
 */
public class TipoPreguntaDaoImpl  implements TipoPreguntaDao{
    
    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<TipoPregunta> listTipoPregunta;

    public TipoPreguntaDaoImpl() {
        cx = new Conexionbd();
    }
    
    

    @Override
    public boolean crearTipoPregunta(TipoPregunta tipre) {
     sql = "INSERT INTO public.tipo_pregunta(nombre) "
                + "VALUES('"+tipre.getNombre()+"')";
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<TipoPregunta> listarTipoPregunta() {
   listTipoPregunta = new ArrayList<>();

        sql = "SELECT * FROM public.tipo_pregunta";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {

                listTipoPregunta.add(new TipoPregunta(Integer.parseInt(rs.getString("id_tipo")), rs.getString("nombre")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(TipoPreguntaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listTipoPregunta;
    
    }

    @Override
    public boolean actualizarTipoPregunta(TipoPregunta tipre) {
   sql="UPDATE public.tipo_pregunta SET"
                +" nombre='"+tipre.getNombre()+"' "
                + " WHERE id_tipo="+tipre.getId_tipo()+"";   
         cx.executeInsertUpdate(sql); 
         
         return false;
    
    }

    @Override
    public boolean eliminarTipoPregunta(int id) {
    sql="DELETE FROM public.tipo_pregunta WHERE id_tipo="+id; 
        return cx.executeInsertUpdate(sql);  
    }
    
}
