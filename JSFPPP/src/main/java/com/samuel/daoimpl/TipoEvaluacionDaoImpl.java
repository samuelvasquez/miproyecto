/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.TipoEvaluacionDao;
import com.samuel.model.TipoEvaluacion;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;



/**
 *
 * @author Shamu
 */
public class TipoEvaluacionDaoImpl implements TipoEvaluacionDao {
     Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<TipoEvaluacion> listTipoEvaluacion;

    public TipoEvaluacionDaoImpl() {
        
         cx = new Conexionbd();
    }
    
    

    @Override
    public boolean crearTipoEvaluacion(TipoEvaluacion tev) {
   sql = "INSERT INTO public.tipo_evaluacion(nombre) "
                + "VALUES('"+tev.getNombre()+"')";
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<TipoEvaluacion> listarTipoEvaluacion() {
    listTipoEvaluacion = new ArrayList<>();

        sql = "SELECT * FROM public.tipo_evaluacion";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {

                listTipoEvaluacion.add(new TipoEvaluacion(Integer.parseInt(rs.getString("id_tipo")), rs.getString("nombre")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(TipoEvaluacionDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listTipoEvaluacion;
    }

    @Override
    public boolean actualizarTipoEvaluacion(TipoEvaluacion tev) {
   sql="UPDATE public.tipo_evaluacion SET"
                +" nombre='"+tev.getNombre()+"' "
                + " WHERE id_tipo="+tev.getId_tipo()+"";   
         cx.executeInsertUpdate(sql); 
         
         return false;
    }

    @Override
    public boolean eliminarTipoEvaluacion(int id) {
    sql="DELETE FROM public.tipo_evaluacion WHERE id_tipo="+id; 
        return cx.executeInsertUpdate(sql);  
   }
    
}
