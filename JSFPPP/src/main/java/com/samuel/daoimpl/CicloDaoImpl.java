
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.CicloDao;
import com.samuel.model.Ciclo;
import com.samuel.model.DocIdentidad;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CicloDaoImpl implements CicloDao {
    
    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Ciclo> listCiclo;
    
     public CicloDaoImpl() {
        cx = new Conexionbd();
    }

    @Override
    public boolean crearCiclo(Ciclo ci) {
        sql = "INSERT INTO public.ciclo(nombre, abreviatura, estado) "
                + "VALUES ('"+ci.getNombre()+"','"+ci.getAbreviatura()+"','"+ci.getEstado()+"')";
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<Ciclo> listarCiclo() {
        listCiclo = new ArrayList<>();
        sql = "SELECT * FROM public.ciclo";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {
                listCiclo.add(new Ciclo(Integer.parseInt(rs.getString("id_ciclo")), rs.getString("nombre"), 
                        rs.getString("abreviatura"), rs.getString("estado")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(CicloDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listCiclo;
    }

    @Override
    public boolean actualizarCiclo(Ciclo ci) {
          sql="UPDATE public.ciclo SET nombre='"+ci.getNombre()+"', abreviatura='"+ci.getAbreviatura()
                  +"',estado='"+ci.getEstado()+"' WHERE id_ciclo="+ci.getId_ciclo();   
       
         return cx.executeInsertUpdate(sql);
    }

    @Override
    public boolean eliminarCiclo(int id) {
        sql="DELETE FROM public.ciclo WHERE id_ciclo='"+id+"'"; 
        return cx.executeInsertUpdate(sql);
    }
    
}
