/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.PersonaDao;
import com.samuel.model.Persona;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eliacer95
 */
public class PersonaDaoImpl implements PersonaDao {

    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Persona> listPersona;

    public PersonaDaoImpl() {
        cx = new Conexionbd();
    }

    @Override
    public boolean crearPersona(Persona pe) {
        boolean status = false;
        
        sql = "INSERT INTO public.persona(nombres, apellidos, id_doc_identidad, nro_doc, telefono, correo, sexo, direccion, foto) "
                + "VALUES('" + pe.getNombres() + "', '" + pe.getApellidos() + "', " + pe.getId_doc_identidad() + ", '" + pe.getNro_doc() + "', "
                //+ "'" + Date.valueOf(pe.getFecha_nac().toString()) + "', "
                + "'" + pe.getTelefono() + "', '" + pe.getCorreo() + "', '" + pe.getSexo() + "', '"
                + pe.getDireccion() + "', '" + pe.getFoto() + "')";
        System.out.println("sql:" + sql);
        status = cx.executeInsertUpdate(sql);
        return status;
    }

    @Override
    public List<Persona> listarPersona() {
        listPersona = new ArrayList<>();

        sql = "SELECT * FROM public.persona";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {
                Date today = new Date(0);
//                if(rs.getString("fecha_nac").equals("")){
//                    today = new Date(0);
//                }else{
//                    today = Date.valueOf(rs.getString("fecha_nac"));
//                }

                listPersona.add(new Persona(Integer.parseInt(rs.getString("id_persona")), rs.getString("nombres"), rs.getString("apellidos"),
                        Integer.parseInt(rs.getString("id_doc_identidad")), rs.getString("nro_doc"), today,
                        rs.getString("telefono"), rs.getString("correo"), rs.getString("sexo"), rs.getString("direccion"), rs.getString("foto")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(PersonaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listPersona;
    }

    @Override
    public boolean actualizarPersona(Persona pe) {
        boolean status = false;
        sql = "UPDATE public.persona SET nombres='" + pe.getNombres() + "', apellidos='" + pe.getApellidos() + "', id_doc_identidad=" + pe.getId_doc_identidad()
                + ", nro_doc='" + pe.getNro_doc() + "', telefono='" + pe.getTelefono() + "', correo='" + pe.getCorreo()
                + "', sexo='" + pe.getSexo() + "', direccion='" + pe.getDireccion() + "', foto='" + pe.getFoto() + "'  WHERE id_persona=" + pe.getId_persona();
        System.out.println("sql:"+sql);
        status = cx.executeInsertUpdate(sql);
        return status;
    }

    @Override
    public boolean eliminarPersona(int id) {
        boolean status = false;
        sql = "DELETE FROM public.persona WHERE id_persona=" + id;
        status = cx.executeInsertUpdate(sql);
        return status;
    }

}
