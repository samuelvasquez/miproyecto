/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.CuestionarioDao;
import com.samuel.model.Cuestionario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Shamu
 */
public class CuestionarioDaoImpl implements CuestionarioDao{
    
     Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Cuestionario> listCuestionario;

    public CuestionarioDaoImpl() {
        
        cx = new Conexionbd();
    }
    
    

    @Override
    public boolean crearCuestionario(Cuestionario cus) {
   sql = "INSERT INTO public.cuestionario( id_tipo, id_pregunta) "
                + "VALUES (" + cus.getId_tipo() + "," + cus.getId_pregunta() + ")";
        
        System.out.println("sql:"+sql);
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<Cuestionario> listarCuestionario() {
    listCuestionario= new ArrayList<>();

        sql = "SELECT * FROM public.cuestionario";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {
                listCuestionario.add(new Cuestionario(Integer.parseInt(rs.getString("id_cuestionario")), Integer.parseInt(rs.getString("id_tipo")),
                       Integer.parseInt(rs.getString("id_pregunta"))));
            }

        } catch (SQLException ex) {
            Logger.getLogger(CuestionarioDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listCuestionario;
    }

    @Override
    public boolean actualizarCuestionario(Cuestionario cus) {
    sql = "UPDATE public.cuestionario SET"
                + " id_tipo=" + cus.getId_tipo() + ", id_pregunta=" + cus.getId_pregunta() + " "
                + " WHERE id_cuestionario=" + cus.getId_cuestionario();
        System.out.println("sql:"+sql);
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public boolean eliminarCuestionario(int id) {
       sql = "DELETE FROM public.cuestionario WHERE id_cuestionario=" + id;
        return cx.executeInsertUpdate(sql);
    }
    
}
