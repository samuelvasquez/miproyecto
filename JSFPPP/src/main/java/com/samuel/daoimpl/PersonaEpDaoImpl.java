
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.PersonaDao;
import com.samuel.dao.PersonaEpDao;
import com.samuel.model.Persona;
import com.samuel.model.PersonaEp;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PersonaEpDaoImpl implements PersonaEpDao {
    
    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<PersonaEp> listPersonaEp;
    
    public PersonaEpDaoImpl() {
        this.cx = new Conexionbd();
    }

    @Override
    public boolean crearPersonaEp(PersonaEp pep) {
       boolean status = false;
        sql = "INSERT INTO public.persona_ep(id_persona, id_escuela, id_cargo, estado) "
                + "VALUES(" + pep.getId_persona() + ", " + pep.getId_escuela() + ", " + pep.getId_cargo() + ", " + pep.getEstado() + ")";
        status = cx.executeInsertUpdate(sql);
        return status;
    }

    @Override
    public List<PersonaEp> listarPersonaEp() {
       listPersonaEp = new ArrayList<>();

        sql = "SELECT * FROM public.persona_ep";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {

               listPersonaEp.add(new PersonaEp(Integer.parseInt(rs.getString("id_persona_ep")),Integer.parseInt(rs.getString("id_persona")),Integer.parseInt(rs.getString("id_escuela")),Integer.parseInt(rs.getString("id_cargo")), rs.getString("estado")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(PersonaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listPersonaEp;
    }

    @Override
    public boolean actualizarPersonaEp(PersonaEp pep) {
        boolean status = false;
        sql = "UPDATE public.persona_ep SET id_persona_ep=" + pep.getId_persona() + ", id_escuela=" + pep.getId_escuela() + ", id_cargo=" + pep.getId_cargo()
                + ", estado='" + pep.getEstado() + "'  WHERE id_persona_ep=" + pep.getId_persona_ep();
        status = cx.executeInsertUpdate(sql);
        return status;
    }

    @Override
    public boolean eliminarPersonaEp(int id) {
         boolean status = false;
        sql = "DELETE FROM public.persona_ep WHERE id_persona_ep=" + id;
        status = cx.executeInsertUpdate(sql);
        return status;
    }

    

    
}
