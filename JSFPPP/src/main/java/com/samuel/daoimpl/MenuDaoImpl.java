
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.MenuDao;
import com.samuel.model.Menu;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class MenuDaoImpl implements MenuDao {
    
     Conexionbd cx;
     String sql;
      ResultSet rs;
    ArrayList<Menu> listMenu;
    
      public MenuDaoImpl() {
        cx = new Conexionbd();
    }

    @Override
    public int crearMenu(Menu me) {
       sql = "INSERT INTO public.menu(nombre, url, imagen, descripcion, estado) "
                + "VALUES('"+me.getNombre()+"', '"+me.getUrl()+"','"+me.getImagen()+"','"+me.getDescripcion()+"', '"+me.getEstado()+"')";
        cx.performKeys(sql); 
        return cx.performKeys(sql);
    }

    @Override
    public List<Menu> listarMenu() {
         listMenu = new ArrayList<>();

       sql = "SELECT * FROM public.menu";
       rs = cx.executeQuery(sql);
       try {
          while (rs.next()) {

                listMenu.add(new Menu(Integer.parseInt(rs.getString("id_modulo")), rs.getString("nombre"), rs.getString("url"),rs.getString("imagen"),rs.getString("descripcion"), rs.getString("estado")));
            }
      } catch (SQLException ex) {
            Logger.getLogger(MenuDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
      }
        return listMenu;
    }

    @Override
    public boolean actualizarMenu(Menu us) {
         sql="UPDATE public.menu SET"
                +" name_cat='"+us.getNombre()+"', url='"+us.getUrl()+"', imagen='"+us.getImagen()+"', descripcion='"+us.getDescripcion()+"', estado='"+us.getEstado()+"' "
                + " WHERE id_modulo="+us.getId_modulo()+"";   
         cx.executeInsertUpdate(sql); 
         
    return false;  
    
    }

    @Override
    public boolean eliminarMenu(int id) {
        sql="DELETE FROM public.menu WHERE id_modulo="+id; 
        return cx.executeInsertUpdate(sql);    
    } 
}
