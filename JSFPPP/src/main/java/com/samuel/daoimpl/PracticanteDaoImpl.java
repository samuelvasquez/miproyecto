package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.EmpresaDao;
import com.samuel.dao.PracticaDao;
import com.samuel.dao.PracticanteDao;
import com.samuel.model.Empresa;
import com.samuel.model.Practica;
import com.samuel.model.Practicante;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PracticanteDaoImpl implements PracticanteDao {

    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Practicante> listPracticante;

    public PracticanteDaoImpl() {
        cx = new Conexionbd();
    }

    
    @Override
    public boolean crearPracticante(Practicante pr) {
        sql = "INSERT INTO public.practicante(id_practicante, id_escuela_profesional, id_ciclo, codigo, estado) "
                + "VALUES (" + pr.getId_practicante() + "," + pr.getId_escuela_profesional() 
                + ", " + pr.getId_ciclo()+ ", '" + pr.getCodigo() + "', '" + pr.getEstado() + "')";
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<Practicante> listarPracticante() {
        listPracticante = new ArrayList<>();

        sql = "SELECT * FROM public.practicante";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {
                listPracticante.add(new Practicante(Integer.parseInt(rs.getString("id_practicante")), 
                        Integer.parseInt(rs.getString("id_escuela_profesional")),
                        Integer.parseInt(rs.getString("id_ciclo")), rs.getString("codigo"), rs.getString("estado")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(PersonaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listPracticante;
    }

    @Override
    public boolean actualizarPracticante(Practicante pr) {
        sql = "UPDATE public.practicante SET"
                + " id_practicante=" + pr.getId_practicante() + ", id_escuela_profesional=" + pr.getId_escuela_profesional()
                + ", id_ciclo=" + pr.getId_ciclo() + ", codigo='" + pr.getCodigo() + "', estado='" + pr.getEstado() + "' "
                + " WHERE id_practicante=" + pr.getId_practicante();
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public boolean eliminarPracticante(int id) {
        sql = "DELETE FROM public.practicante WHERE id_practicante=" + id;
        return cx.executeInsertUpdate(sql);
    }

}
