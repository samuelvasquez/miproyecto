/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.RolDao;

import com.samuel.model.Rol;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Shamu
 */
public class RolDaoImpl implements RolDao {
    
    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Rol> listRol;

    public RolDaoImpl() {
         cx = new Conexionbd();
    }
    
    

    @Override
    public boolean crearRol(Rol rol) {
        sql = "INSERT INTO public.rol( nombre,descripcion, estado) "
                + "VALUES('" + rol.getNombre()+ "', '" + rol.getDescripcion() +"', '"+rol.getEstado() + "')";
      return cx.executeInsertUpdate(sql);
    }

    

    @Override
    public List<Rol> listarRol() {
        listRol = new ArrayList<>();

        sql = "SELECT * FROM public.rol";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {

                listRol.add(new Rol(Integer.parseInt(rs.getString("id_rol")), rs.getString("nombre"), rs.getString("descripcion"), rs.getString("estado")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(RolDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listRol;
    }

    @Override
    public boolean actualizarRol(Rol rol) {
        sql = "UPDATE public.rol SET"
                + " nombre='" + rol.getNombre() + "', descripcion='" + rol.getDescripcion() + "', estado='" + rol.getEstado() + "' "
                + " WHERE id_rol=" + rol.getId_rol() + "";
        cx.executeInsertUpdate(sql);

        return false;
    }

    @Override
    public boolean eliminarRol(int id) {
      sql = "DELETE FROM public.rol WHERE id_rol= "+ id;
        return cx.executeInsertUpdate(sql);}
    
}
