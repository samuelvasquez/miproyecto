package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.CargoDao;
import com.samuel.dao.TipoDocumentoDao;
import com.samuel.model.Cargo;
import com.samuel.model.TipoDocumento;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TipoDocumentoDaoImpl implements TipoDocumentoDao {

    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<TipoDocumento> listTD;

    public TipoDocumentoDaoImpl() {
        cx = new Conexionbd();
    }

    @Override
    public boolean crearTipoDocumento(TipoDocumento tp) {
        sql = "INSERT INTO public.tipo_documento(nombre, abreviatura, max_nro_documento) "
                + "VALUES('" + tp.getNombre() + "', '" + tp.getAbreviatura() + "'," + tp.getMax_nro_documento() + ")";
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<TipoDocumento> listarTipoDocumento() {
        listTD = new ArrayList<>();

        sql = "SELECT * FROM public.tipo_documento";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {
                listTD.add(new TipoDocumento(Integer.parseInt(rs.getString("id_tipo_documento")), rs.getString("nombre"),
                        rs.getString("abreviatura"), Integer.parseInt(rs.getString("max_nro_documento"))));
            }

        } catch (SQLException ex) {
            Logger.getLogger(PersonaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listTD;
    }

    @Override
    public boolean actualizarTipoDocumento(TipoDocumento tp) {
        sql = "UPDATE public.tipo_documento SET"
                + " nombre='" + tp.getNombre() + "', abreviatura='" + tp.getAbreviatura() + "', max_nro_documento=" + tp.getMax_nro_documento()
                + " WHERE id_tipo_documento=" + tp.getId_tipo_documento() + "";
        cx.executeInsertUpdate(sql);

        return false;
    }

    @Override
    public boolean eliminarTipoDocumento(int id) {
        sql = "DELETE FROM public.tipo_documento WHERE id_tipo_documento= " + id;
        return cx.executeInsertUpdate(sql);
    }

}
