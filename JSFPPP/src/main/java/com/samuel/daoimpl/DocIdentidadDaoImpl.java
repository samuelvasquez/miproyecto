package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.DocIdentidadDao;
import com.samuel.model.DocIdentidad;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DocIdentidadDaoImpl implements DocIdentidadDao {

    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<DocIdentidad> listDI;

    public DocIdentidadDaoImpl() {
        cx = new Conexionbd();
    }

    @Override
    public boolean crearDocIdentidad(DocIdentidad di) {
        sql = "INSERT INTO public.doc_identidad(nombre, abreviatura) "
                + "VALUES('" + di.getNombre() + "', '" + di.getAbreviatura() + "')";
        System.out.println("sql:" + sql);
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<DocIdentidad> listarDocIdentidad() {
        listDI = new ArrayList<>();

        sql = "SELECT * FROM public.doc_identidad";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {
                listDI.add(new DocIdentidad(Integer.parseInt(rs.getString("id_doc_identidad")), rs.getString("nombre"), rs.getString("abreviatura")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(DocIdentidadDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listDI;
    }

    @Override
    public boolean actualizarDocIdentidad(DocIdentidad di) {
        sql = "UPDATE public.doc_identidad SET"
                + " nombre='" + di.getNombre() + "', abreviatura='" + di.getAbreviatura() + "'"
                + " WHERE id_doc_identidad=" + di.getId_doc_identidad();
        System.out.println("sql:" + sql);
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public boolean eliminarDocIdentidad(int id) {
        sql = "DELETE FROM public.doc_identidad WHERE id_doc_identidad=" + id;
        return cx.executeInsertUpdate(sql);
    }

}
