/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.EvaluacionDao;
import com.samuel.model.Evaluacion;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Shamu
 */
public class EvaluacionDaoImpl implements EvaluacionDao{
     Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Evaluacion> listEvaluacion;

    public EvaluacionDaoImpl() {
         cx = new Conexionbd();
    }
    
    
    

    @Override
    public boolean crearEvaluacion(Evaluacion ev) {
      sql = "INSERT INTO public.evaluacion(id_practica, total_puntaje, id_cuestionario) "
                + "VALUES('"+ev.getId_practica()+"', '"+ev.getTotal_puntaje()+"', '"+ev.getId_cuestionario()+"')";
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<Evaluacion> listarEvaluacion() {
     listEvaluacion = new ArrayList<>();

        try {
            while (rs.next()) {
                Date today = new Date(0);
//                if(rs.getString("fecha_nac").equals("")){
//                    today = new Date(0);
//                }else{
//                    today = Date.valueOf(rs.getString("fecha_nac"));
//                }

                listEvaluacion.add(new Evaluacion(Integer.parseInt(rs.getString("id_evaluacion")), Integer.parseInt(rs.getString("id_practica")), today, Integer.parseInt(rs.getString("total_puntaje")),
                        
                        Integer.parseInt(rs.getString("id_cuestionario"))));
            }

        } catch (SQLException ex) {
            Logger.getLogger(EvaluacionDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listEvaluacion;
    
    }

    @Override
    public boolean actualizarEvaluacion(Evaluacion ev) {
     boolean status = false;
        sql = "UPDATE public.evaluacion SET id_practica='" + ev.getId_practica() + "', total_puntaje='" + ev.getTotal_puntaje() + "', id_cuestionario=" + ev.getId_cuestionario();
        System.out.println("sql:"+sql);
        status = cx.executeInsertUpdate(sql);
        return status;
    }

    @Override
    public boolean eliminarEvaluacion(int id) {
    sql="DELETE FROM public.evaluacion WHERE id_evaluacion="+id; 
        return cx.executeInsertUpdate(sql);  
    }
    
}
