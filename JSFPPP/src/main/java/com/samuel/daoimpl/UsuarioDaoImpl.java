/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.UsuarioDao;
import com.samuel.model.Usuario;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class UsuarioDaoImpl implements UsuarioDao{
    
    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Usuario> listUsuario;

    public UsuarioDaoImpl() {
        cx = new Conexionbd();
    }
    
    
    @Override
    public boolean crearUsuario(Usuario us) {
        sql = "INSERT INTO public.usuario(id_rol, login, clave, estado) "
                + "VALUES(" + us.getId_rol() + ",'" + us.getLogin()+ "', '" + us.getClave() +"', '"+us.getEstado() + "')";
      return cx.executeInsertUpdate(sql);
       
    }

    @Override
    public List<Usuario> listarUsuario() {
       listUsuario = new ArrayList<>();

        sql = "SELECT * FROM public.usuario";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {

                listUsuario.add(new Usuario(Integer.parseInt(rs.getString("id_usuario")), Integer.parseInt(rs.getString("id_rol")), rs.getString("login"),rs.getString("clave"), rs.getString("estado")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(UsuarioDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listUsuario;
    }
   

    @Override
    public boolean actualizarUsuario(Usuario us) {
        sql = "UPDATE public.usuario SET"
                + " id_rol=" + us.getId_rol()+ ", login='" + us.getLogin()+ "', clave='"+us.getLogin()+"', estado='" + us.getEstado() + "' "
                + " WHERE id_usuario=" + us.getId_usuario() + "";
        cx.executeInsertUpdate(sql);

        return false;
    }

    @Override
    public boolean eliminarUsuario(int id) {
        sql = "DELETE FROM public.usuario WHERE id_usuario= "+ id;
        return cx.executeInsertUpdate(sql);
    }
    
}
