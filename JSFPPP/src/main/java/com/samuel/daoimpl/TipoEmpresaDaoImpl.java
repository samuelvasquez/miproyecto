package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.TipoEmpresaDao;
import com.samuel.model.TipoEmpresa;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TipoEmpresaDaoImpl implements TipoEmpresaDao {

    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<TipoEmpresa> listTE;

    public TipoEmpresaDaoImpl() {
        cx = new Conexionbd();
    }

    @Override
    public boolean crearTipoEmpresa(TipoEmpresa te) {
        sql = "INSERT INTO public.tipo_empresa(nombre) "
                + "VALUES('" + te.getNombre() + "')";
        System.out.println("sql:" + sql);
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<TipoEmpresa> listarTipoEmpresa() {
        listTE = new ArrayList<>();

        sql = "SELECT * FROM public.tipo_empresa";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {
                listTE.add(new TipoEmpresa(Integer.parseInt(rs.getString("id_tipo_empresa")), rs.getString("nombre")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(TipoEmpresaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listTE;
    }

    @Override
    public boolean actualizarTipoEmpresa(TipoEmpresa di) {
        sql = "UPDATE public.tipo_empresa SET"
                + " nombre='" + di.getNombre() + "' WHERE id_tipo_empresa=" + di.getId_tipo_empresa();
        System.out.println("sql:" + sql);
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public boolean eliminarTipoEmpresa(int id) {
        sql = "DELETE FROM public.tipo_empresa WHERE id_tipo_empresa=" + id;
        return cx.executeInsertUpdate(sql);
    }

}
