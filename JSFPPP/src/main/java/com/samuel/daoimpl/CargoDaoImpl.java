package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.CargoDao;
import com.samuel.model.Cargo;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CargoDaoImpl implements CargoDao {

    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Cargo> listCargo;

    public CargoDaoImpl() {
        cx = new Conexionbd();
    }

    @Override
    public boolean crearCargo(Cargo ca) {
        sql = "INSERT INTO public.cargo(nombre, descripcion, estado) "
                + "VALUES('" + ca.getNombre() + "', '" + ca.getDescripcion() +"', '"+ca.getEstado() + "')";
       return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<Cargo> listarCargo() {
        listCargo = new ArrayList<>();

        sql = "SELECT * FROM public.cargo";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {

                listCargo.add(new Cargo(Integer.parseInt(rs.getString("id_cargo")), rs.getString("nombre"), rs.getString("descripcion"), rs.getString("estado")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(CargoDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listCargo;
    }

    @Override
    public boolean actualizarCargo(Cargo ca) {
        sql = "UPDATE public.cargo SET"
                + " nombre='" + ca.getNombre() + "', descripcion='" + ca.getDescripcion() + "', estado='" + ca.getEstado() + "' "
                + " WHERE id_cargo=" + ca.getId_cargo() + "";
        cx.executeInsertUpdate(sql);

        return false;
    }

    @Override
    public boolean eliminarCargo(int id) {
        sql = "DELETE FROM public.cargo WHERE id_cargo= "+ id;
        return cx.executeInsertUpdate(sql);
    }

}
