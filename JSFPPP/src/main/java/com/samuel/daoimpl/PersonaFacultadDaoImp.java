
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.PersonaFacultadDao;
import com.samuel.model.PersonaFacultad;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class PersonaFacultadDaoImp implements PersonaFacultadDao {
    
    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<PersonaFacultad> listPersonaFacultad;
    
    public PersonaFacultadDaoImp() {
        cx = new Conexionbd();
    }
    
    @Override
    public boolean crearPersonaFacultad(PersonaFacultad pf) {
        
         boolean status = false;
        sql = "INSERT INTO public.persona_facultad(id_persona, id_facultad, id_cargo, estado) "
                + "VALUES(" + pf.getId_persona() + ", " + pf.getId_facultad() + ", " + pf.getId_cargo() + ", '" + pf.getEstado() + "')";
        status = cx.executeInsertUpdate(sql);
        return status;
       
    }

    @Override
    public List<PersonaFacultad> listarPersonaFacultad() {
        
        listPersonaFacultad = new ArrayList<>();

        sql = "SELECT * FROM public.persona_facultad";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {

                listPersonaFacultad.add(new PersonaFacultad(Integer.parseInt(rs.getString("id_persona_facultad")), Integer.parseInt(rs.getString("id_persona")), 
                        Integer.parseInt(rs.getString("id_facultad")), Integer.parseInt(rs.getString("id_cargo")),rs.getString("estado")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(PersonaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listPersonaFacultad;
       
    }

    @Override
    public boolean actualizarPersonaFacultad(PersonaFacultad pf) {
        boolean status = false;
        sql = "UPDATE public.persona_facultad SET id_persona="+ pf.getId_persona() + ",  id_facultad=" + pf.getId_facultad() + ", id_cargo=" + pf.getId_cargo() + ", estado='" + pf.getEstado() +
                "'  WHERE id_persona_facultad=" + pf.getId_persona_facultad();
        status = cx.executeInsertUpdate(sql);
        return status;
    }

    @Override
    public boolean eliminarPersonaFacultad(int id) {
         boolean status = false;
        sql = "DELETE FROM public.persona_facultad WHERE id_persona_facultad=" + id;
        status = cx.executeInsertUpdate(sql);
        return status;
    }
    
}
