/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.PreguntaDao;
import com.samuel.model.Pregunta;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Shamu
 */
public class PreguntaDaoImpl implements PreguntaDao {
    
    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Pregunta> listPregunta;

    public PreguntaDaoImpl() {
        
        cx = new Conexionbd();
    }
    
    

    @Override
    public boolean crearPregunta(Pregunta pr) {
        sql = "INSERT INTO public.pregunta(nombre, id_tipo) "
                + "VALUES('"+pr.getNombre()+"',"+pr.getId_tipo()+")";
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<Pregunta> listarPregunta() {
      listPregunta = new ArrayList<>();

        sql = "SELECT * FROM public.pregunta";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {

               listPregunta.add(new Pregunta(Integer.parseInt(rs.getString("id_pregunta")), rs.getString("nombre"),
                       Integer.parseInt(rs.getString("id_tipo"))));
            }

        } catch (SQLException ex) {
            Logger.getLogger(PreguntaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listPregunta;
    }

    @Override
    public boolean actualizarPregunta(Pregunta pr) {
    sql = "UPDATE public.pregunta SET"
                + " nombre='" + pr.getNombre() + "', id_tipo=" + pr.getId_tipo() + " "
                + " WHERE id_pregunta=" + pr.getId_pregunta();
        System.out.println("sql:"+sql);
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public boolean eliminarPregunta(int id) {
   sql = "DELETE FROM public.pregunta WHERE id_pregunta=" + id;
        return cx.executeInsertUpdate(sql);
    }
    
}
