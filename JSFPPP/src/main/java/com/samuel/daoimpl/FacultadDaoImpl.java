/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import java.sql.ResultSet;
import com.samuel.dao.FacultadDao;
import com.samuel.model.Facultad;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eliacer95
 */
public class FacultadDaoImpl implements FacultadDao{
    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Facultad> listFacultad;

    public FacultadDaoImpl() {
        cx = new Conexionbd();
    }
    

    @Override
    public boolean crearFacultad(Facultad fa) {
        sql = "INSERT INTO public.facultad(nombre, abreviatura, estado) "
                + "VALUES('"+fa.getNombre()+"', '"+fa.getAbreviatura()+"', '"+fa.getEstado()+"')";
        return cx.executeInsertUpdate(sql);
    }

    @Override
    public List<Facultad> listarFacultad() {
         listFacultad = new ArrayList<>();

        sql = "SELECT * FROM public.facultad";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {

                listFacultad.add(new Facultad(Integer.parseInt(rs.getString("id_facultad")), rs.getString("nombre"), rs.getString("abreviatura"), rs.getString("estado")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(PersonaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listFacultad;
    }

    @Override
    public boolean actualizarFacultad(Facultad fa) {
           sql="UPDATE public.facultad SET"
                +" nombre='"+fa.getNombre()+"', abreviatura='"+fa.getAbreviatura()+"', estado='"+fa.getEstado()+ "' "
                + " WHERE id_facultad="+fa.getId_facultad()+"";   
         cx.executeInsertUpdate(sql); 
         
         return false;
    }
    @Override
    public boolean eliminarFacultad(int id) {
        sql="DELETE FROM public.facultad WHERE id_facultad="+id; 
        return cx.executeInsertUpdate(sql);  
    }
    
}
