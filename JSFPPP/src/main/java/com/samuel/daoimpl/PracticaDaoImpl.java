
package com.samuel.daoimpl;

import com.samuel.configdb.Conexionbd;
import com.samuel.dao.PracticaDao;
import com.samuel.model.Practica;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
    
public class PracticaDaoImpl implements PracticaDao {
    
    
    Conexionbd cx;
    String sql;
    ResultSet rs;
    ArrayList<Practica> listPractica;
    
    public PracticaDaoImpl() {
        cx = new Conexionbd();
    }

    @Override
    public boolean crearPractica(Practica pr) {
         boolean status = false;
            sql = "INSERT INTO public.practica(id_practicante, id_empresa,  fecha_ini,fecha_fin, fecha_reg) "
                    + "VALUES(" + pr.getId_practicante() + ", " + pr.getId_empresa() + ", '" + pr.getFecha_ini() + "', "
                + " '" + pr.getFecha_fin() + "', '" + pr.getFecha_reg() + "')";
            
            
        status = cx.executeInsertUpdate(sql);
        return status;
    }

    @Override
    public List<Practica> listarPractica() {
        listPractica = new ArrayList<>();

        sql = "SELECT * FROM public.practica";
        rs = cx.executeQuery(sql);
        try {
            while (rs.next()) {

               listPractica.add(new Practica(Integer.parseInt(rs.getString("id_practica")), Integer.parseInt(rs.getString("id_practicante")),Integer.parseInt(rs.getString("id_empresa")), rs.getString("fecha_ini"),rs.getString("fecha_fin"),rs.getString("fecha_reg")));
            }

        } catch (SQLException ex) {
            Logger.getLogger(PersonaDaoImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listPractica;
    }

    @Override
    public boolean actualizarPractica(Practica pr) {
        sql = "UPDATE public.practica SET id_practicante="+ pr.getId_practicante() + ",  id_empresa=" + pr.getId_empresa() + ", fecha_ini='" + pr.getFecha_ini() + 
                "', fecha_fin='" + pr.getFecha_fin() + "', fecha_reg='" + pr.getFecha_reg()+ 
                "'  WHERE id_practica=" + pr.getId_practica();
       cx.executeInsertUpdate(sql);
      return false;
    }

    @Override
    public boolean eliminarPractica(int id) {
              sql="DELETE FROM public.practica WHERE id_practica="+id; 
        return cx.executeInsertUpdate(sql);
    }
    
}
