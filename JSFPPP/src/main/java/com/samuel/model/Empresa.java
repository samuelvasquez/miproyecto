package com.samuel.model;

import java.sql.Date;

public class Empresa {

    int id_empresa;
    int id_tipo_empresa;
    String nombre;
    String ruc;
    String direccion;
    String estado;

    public Empresa(int id_empresa, int id_tipo_empresa, String nombre, String ruc, String direccion, String estado) {
        this.id_empresa = id_empresa;
        this.id_tipo_empresa = id_tipo_empresa;
        this.nombre = nombre;
        this.ruc = ruc;
        this.direccion = direccion;
        this.estado = estado;
    }

    public Empresa() {
    }

    public int getId_empresa() {
        return id_empresa;
    }

    public void setId_empresa(int id_empresa) {
        this.id_empresa = id_empresa;
    }

    public int getId_tipo_empresa() {
        return id_tipo_empresa;
    }

    public void setId_tipo_empresa(int id_tipo_empresa) {
        this.id_tipo_empresa = id_tipo_empresa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String dirección) {
        this.direccion = dirección;
    }
    
    

}
