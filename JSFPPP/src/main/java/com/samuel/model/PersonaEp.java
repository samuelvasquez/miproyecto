
package com.samuel.model;


public class PersonaEp {
    
    int id_persona_ep;
    int id_persona;
    int id_escuela;
    int id_cargo;
    String estado;

    public PersonaEp(int id_persona_ep, int id_persona, int id_escuela, int id_cargo, String estado) {
        this.id_persona_ep = id_persona_ep;
        this.id_persona = id_persona;
        this.id_escuela = id_escuela;
        this.id_cargo = id_cargo;
        this.estado = estado;
    }

    public PersonaEp() {
    }

    public int getId_persona_ep() {
        return id_persona_ep;
    }

    public void setId_persona_ep(int id_persona_ep) {
        this.id_persona_ep = id_persona_ep;
    }

    public int getId_persona() {
        return id_persona;
    }

    public void setId_persona(int id_persona) {
        this.id_persona = id_persona;
    }

    public int getId_escuela() {
        return id_escuela;
    }

    public void setId_escuela(int id_escuela) {
        this.id_escuela = id_escuela;
    }

    public int getId_cargo() {
        return id_cargo;
    }

    public void setId_cargo(int id_cargo) {
        this.id_cargo = id_cargo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
    
}
