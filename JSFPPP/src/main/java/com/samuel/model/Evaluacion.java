/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.model;

import java.sql.Date;

/**
 *
 * @author Shamu
 */
public class Evaluacion {
    int id_evaluacion;
    int id_practica;
    Date fecha;
    int total_puntaje;
    int id_cuestionario;

    public Evaluacion(int id_evaluacion, int id_practica, Date fecha, int total_puntaje, int id_cuestionario) {
        this.id_evaluacion = id_evaluacion;
        this.id_practica = id_practica;
        this.fecha = fecha;
        this.total_puntaje = total_puntaje;
        this.id_cuestionario = id_cuestionario;
    }

    public Evaluacion() {
    }

    public int getId_evaluacion() {
        return id_evaluacion;
    }

    public void setId_evaluacion(int id_evaluacion) {
        this.id_evaluacion = id_evaluacion;
    }

    public int getId_practica() {
        return id_practica;
    }

    public void setId_practica(int id_practica) {
        this.id_practica = id_practica;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getTotal_puntaje() {
        return total_puntaje;
    }

    public void setTotal_puntaje(int total_puntaje) {
        this.total_puntaje = total_puntaje;
    }

    public int getId_cuestionario() {
        return id_cuestionario;
    }

    public void setId_cuestionario(int id_cuestionario) {
        this.id_cuestionario = id_cuestionario;
    }
    
    
    
}
