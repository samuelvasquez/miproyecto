
package com.samuel.model;



public class Practica {
    
    int id_practica;
    int id_practicante;
    int id_empresa;
    String fecha_ini;
    String fecha_fin;
    String fecha_reg;

    public Practica(int id_practica, int id_practicante, int id_empresa, String fecha_ini, String fecha_fin, String fecha_reg) {
        this.id_practica = id_practica;
        this.id_practicante = id_practicante;
        this.id_empresa = id_empresa;
        this.fecha_ini = fecha_ini;
        this.fecha_fin = fecha_fin;
        this.fecha_reg = fecha_reg;
    }

    public Practica() {
    }

    public int getId_practica() {
        return id_practica;
    }

    public void setId_practica(int id_practica) {
        this.id_practica = id_practica;
    }

    public int getId_practicante() {
        return id_practicante;
    }

    public void setId_practicante(int id_practicante) {
        this.id_practicante = id_practicante;
    }

    public int getId_empresa() {
        return id_empresa;
    }

    public void setId_empresa(int id_empresa) {
        this.id_empresa = id_empresa;
    }

    public String getFecha_ini() {
        return fecha_ini;
    }

    public void setFecha_ini(String fecha_ini) {
        this.fecha_ini = fecha_ini;
    }

    public String getFecha_fin() {
        return fecha_fin;
    }

    public void setFecha_fin(String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }

    public String getFecha_reg() {
        return fecha_reg;
    }

    public void setFecha_reg(String fecha_reg) {
        this.fecha_reg = fecha_reg;
    }

  
}