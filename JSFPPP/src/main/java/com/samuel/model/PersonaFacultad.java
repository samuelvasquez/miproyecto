
package com.samuel.model;

public class PersonaFacultad {
   int id_persona_facultad; 
   int id_persona;
   int id_facultad;
   int id_cargo;
   String estado;

    public PersonaFacultad(int id_persona_facultad, int id_persona, int id_facultad, int id_cargo, String estado) {
        this.id_persona_facultad = id_persona_facultad;
        this.id_persona = id_persona;
        this.id_facultad = id_facultad;
        this.id_cargo = id_cargo;
        this.estado = estado;
    }

    public PersonaFacultad() {
    }

    public int getId_persona_facultad() {
        return id_persona_facultad;
    }

    public void setId_persona_facultad(int id_persona_facultad) {
        this.id_persona_facultad = id_persona_facultad;
    }

    public int getId_persona() {
        return id_persona;
    }

    public void setId_persona(int id_persona) {
        this.id_persona = id_persona;
    }

    public int getId_facultad() {
        return id_facultad;
    }

    public void setId_facultad(int id_facultad) {
        this.id_facultad = id_facultad;
    }

    public int getId_cargo() {
        return id_cargo;
    }

    public void setId_cargo(int id_cargo) {
        this.id_cargo = id_cargo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
   
   
   
   
}
