
package com.samuel.model;


public class TipoDocumento {
    int id_tipo_documento;
    String nombre;
    String abreviatura;
    int max_nro_documento;

    public TipoDocumento(int id_tipo_documento, String nombre, String abreviatura, int max_nro_documento) {
        this.id_tipo_documento = id_tipo_documento;
        this.nombre = nombre;
        this.abreviatura = abreviatura;
        this.max_nro_documento = max_nro_documento;
    }

    public TipoDocumento() {
    }

    public int getId_tipo_documento() {
        return id_tipo_documento;
    }

    public void setId_tipo_documento(int id_tipo_documento) {
        this.id_tipo_documento = id_tipo_documento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public int getMax_nro_documento() {
        return max_nro_documento;
    }

    public void setMax_nro_documento(int max_nro_documento) {
        this.max_nro_documento = max_nro_documento;
    }
    
    
    
}
