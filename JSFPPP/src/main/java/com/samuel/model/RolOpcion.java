package com.samuel.model;

public class RolOpcion {
    int id_rol_opcion;
    int id_rol;
    int id_submenu;
    int id_crud;
    String estado; 

    public RolOpcion(int id_rol_opcion, int id_rol, int id_submenu, int id_crud, String estado) {
        this.id_rol_opcion = id_rol_opcion;
        this.id_rol = id_rol;
        this.id_submenu = id_submenu;
        this.id_crud = id_crud;
        this.estado = estado;
    }

    public RolOpcion() {
    }

    public int getId_rol_opcion() {
        return id_rol_opcion;
    }

    public void setId_rol_opcion(int id_rol_opcion) {
        this.id_rol_opcion = id_rol_opcion;
    }

    public int getId_rol() {
        return id_rol;
    }

    public void setId_rol(int id_rol) {
        this.id_rol = id_rol;
    }

    public int getId_submenu() {
        return id_submenu;
    }

    public void setId_submenu(int id_submenu) {
        this.id_submenu = id_submenu;
    }

    public int getId_crud() {
        return id_crud;
    }

    public void setId_crud(int id_crud) {
        this.id_crud = id_crud;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
}
