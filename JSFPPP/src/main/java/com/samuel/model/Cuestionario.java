/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.model;

/**
 *
 * @author Shamu
 */
public class Cuestionario {
    
    int id_cuestionario;
    int id_tipo;
    int id_pregunta;

    public Cuestionario(int id_cuestionario, int id_tipo, int id_pregunta) {
        this.id_cuestionario = id_cuestionario;
        this.id_tipo = id_tipo;
        this.id_pregunta = id_pregunta;
    }

    public Cuestionario() {
    }

    public int getId_cuestionario() {
        return id_cuestionario;
    }

    public void setId_cuestionario(int id_cuestionario) {
        this.id_cuestionario = id_cuestionario;
    }

    public int getId_tipo() {
        return id_tipo;
    }

    public void setId_tipo(int id_tipo) {
        this.id_tipo = id_tipo;
    }

    public int getId_pregunta() {
        return id_pregunta;
    }

    public void setId_pregunta(int id_pregunta) {
        this.id_pregunta = id_pregunta;
    }
    
    
    
}
