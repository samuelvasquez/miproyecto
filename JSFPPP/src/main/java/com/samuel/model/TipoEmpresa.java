
package com.samuel.model;


public class TipoEmpresa {
    
    int id_tipo_empresa;
   String  nombre;

    public TipoEmpresa(int id_tipo_empresa, String nombre) {
        this.id_tipo_empresa = id_tipo_empresa;
        this.nombre = nombre;
    }

    public TipoEmpresa() {
    }

    public int getId_tipo_empresa() {
        return id_tipo_empresa;
    }

    public void setId_tipo_empresa(int id_tipo_empresa) {
        this.id_tipo_empresa = id_tipo_empresa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
   
    
}
