
package com.samuel.model;

import java.sql.Date;


public class Persona {
    
    int id_persona;
    String nombres;
    String apellidos;
    int id_doc_identidad;
    String nro_doc;
    Date fecha_nac;
    String telefono;
    String correo;
    String sexo;
    String direccion;
    String foto;

    public Persona(int id_persona, String nombres, String apellidos, int id_doc_identidad, String nro_doc, Date fecha_nac, String telefono, String correo, String sexo, String direccion, String foto) {
        this.id_persona = id_persona;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.id_doc_identidad = id_doc_identidad;
        this.nro_doc = nro_doc;
        this.fecha_nac = fecha_nac;
        this.telefono = telefono;
        this.correo = correo;
        this.sexo = sexo;
        this.direccion = direccion;
        this.foto = foto;
    }

    public Persona() {
    }

    public int getId_persona() {
        return id_persona;
    }

    public void setId_persona(int id_persona) {
        this.id_persona = id_persona;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getId_doc_identidad() {
        return id_doc_identidad;
    }

    public void setId_doc_identidad(int id_doc_identidad) {
        this.id_doc_identidad = id_doc_identidad;
    }

    public String getNro_doc() {
        return nro_doc;
    }

    public void setNro_doc(String nro_doc) {
        this.nro_doc = nro_doc;
    }

    public Date getFecha_nac() {
        return fecha_nac;
    }

    public void setFecha_nac(Date fecha_nac) {
        this.fecha_nac = fecha_nac;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
