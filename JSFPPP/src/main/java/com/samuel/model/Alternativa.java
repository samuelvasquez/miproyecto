/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.model;

/**
 *
 * @author Shamu
 */
public class Alternativa {
    
    int id_alternativa;
    String nombre;
    int id_pregunta;

    public Alternativa(int id_alternativa, String nombre, int id_pregunta) {
        this.id_alternativa = id_alternativa;
        this.nombre = nombre;
        this.id_pregunta = id_pregunta;
    }

    public Alternativa() {
    }

    public int getId_alternativa() {
        return id_alternativa;
    }

    public void setId_alternativa(int id_alternativa) {
        this.id_alternativa = id_alternativa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId_pregunta() {
        return id_pregunta;
    }

    public void setId_pregunta(int id_pregunta) {
        this.id_pregunta = id_pregunta;
    }
    
    
    
}
