
package com.samuel.model;

public class Usuario {
    
    int id_usuario;
    int id_rol;
    String  login;
    String clave;
    String estado;

    public Usuario(int id_usuario, int id_rol, String login, String clave, String estado) {
        this.id_usuario = id_usuario;
        this.id_rol = id_rol;
        this.login = login;
        this.clave = clave;
        this.estado = estado;
    }

    public Usuario() {
    }

    public int getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(int id_usuario) {
        this.id_usuario = id_usuario;
    }

    public int getId_rol() {
        return id_rol;
    }

    public void setId_rol(int id_rol) {
        this.id_rol = id_rol;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
    
}
