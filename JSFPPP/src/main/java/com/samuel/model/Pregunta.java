/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.model;

/**
 *
 * @author Shamu
 */
public class Pregunta {
    
    int id_pregunta;
    String nombre;
    int id_tipo;

    public Pregunta(int id_pregunta, String nombre, int id_tipo) {
        this.id_pregunta = id_pregunta;
        this.nombre = nombre;
        this.id_tipo = id_tipo;
    }

    public Pregunta() {
    }

    public int getId_pregunta() {
        return id_pregunta;
    }

    public void setId_pregunta(int id_pregunta) {
        this.id_pregunta = id_pregunta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId_tipo() {
        return id_tipo;
    }

    public void setId_tipo(int id_tipo) {
        this.id_tipo = id_tipo;
    }
    
    
    
}
