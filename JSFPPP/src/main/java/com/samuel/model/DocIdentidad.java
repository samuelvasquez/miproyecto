
package com.samuel.model;


public class DocIdentidad {
    int id_doc_identidad;
    String nombre;
    String abreviatura;

    public DocIdentidad(int id_doc_identidad, String nombre, String abreviatura) {
        this.id_doc_identidad = id_doc_identidad;
        this.nombre = nombre;
        this.abreviatura = abreviatura;
    }

    public DocIdentidad() {
    }

    public int getId_doc_identidad() {
        return id_doc_identidad;
    }

    public void setId_doc_identidad(int id_doc_identidad) {
        this.id_doc_identidad = id_doc_identidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }
    
}
