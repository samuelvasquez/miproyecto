
package com.samuel.model;


public class Evidencia {
    
    
    int id_evidencia;
    int id_practica;
    int id_tipo_documento;
    String url_archivo;
    int nro_documento;

    public Evidencia(int id_evidencia, int id_practica, int id_tipo_documento, String url_archivo, int nro_documento) {
        this.id_evidencia = id_evidencia;
        this.id_practica = id_practica;
        this.id_tipo_documento = id_tipo_documento;
        this.url_archivo = url_archivo;
        this.nro_documento = nro_documento;
    }

    public Evidencia() {
    }

    public int getId_evidencia() {
        return id_evidencia;
    }

    public void setId_evidencia(int id_evidencia) {
        this.id_evidencia = id_evidencia;
    }

    public int getId_practica() {
        return id_practica;
    }

    public void setId_practica(int id_practica) {
        this.id_practica = id_practica;
    }

    public int getId_tipo_documento() {
        return id_tipo_documento;
    }

    public void setId_tipo_documento(int id_tipo_documento) {
        this.id_tipo_documento = id_tipo_documento;
    }

    public String getUrl_archivo() {
        return url_archivo;
    }

    public void setUrl_archivo(String url_archivo) {
        this.url_archivo = url_archivo;
    }

    public int getNro_documento() {
        return nro_documento;
    }

    public void setNro_documento(int nro_documento) {
        this.nro_documento = nro_documento;
    }
    
    
    
}
