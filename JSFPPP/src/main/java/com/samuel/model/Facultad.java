
package com.samuel.model;


public class Facultad {
    int id_facultad;
    String nombre, abreviatura, estado;

    public Facultad(int id_facultad, String nombre, String abreviatura, String estado) {
        this.id_facultad = id_facultad;
        this.nombre = nombre;
        this.abreviatura = abreviatura;
        this.estado = estado;
    }

    public Facultad() {
    }

    public int getId_facultad() {
        return id_facultad;
    }

    public void setId_facultad(int id_facultad) {
        this.id_facultad = id_facultad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAbreviatura() {
        return abreviatura;
    }

    public void setAbreviatura(String abreviatura) {
        this.abreviatura = abreviatura;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
    
}
