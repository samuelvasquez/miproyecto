
package com.samuel.model;


public class SubMenu {
    
    int id_submenu;
    int id_modulo;
    String nombre;
    String url;
    String imagen;
    String descripcion;
    String estado;

    public SubMenu(int id_submenu, int id_modulo, String nombre, String url, String imagen, String descripcion, String estado) {
        this.id_submenu = id_submenu;
        this.id_modulo = id_modulo;
        this.nombre = nombre;
        this.url = url;
        this.imagen = imagen;
        this.descripcion = descripcion;
        this.estado = estado;
    }

    public SubMenu() {
    }

    public int getId_submenu() {
        return id_submenu;
    }

    public void setId_submenu(int id_submenu) {
        this.id_submenu = id_submenu;
    }

    public int getId_modulo() {
        return id_modulo;
    }

    public void setId_modulo(int id_modulo) {
        this.id_modulo = id_modulo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    
    
    
    
}
