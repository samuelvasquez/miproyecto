
package com.samuel.model;


public class Practicante {
    
    
    int id_practicante;
    int id_escuela_profesional;
    int id_ciclo;
    String codigo;
    String estado;

    public Practicante(int id_practicante, int id_escuela_profesional, int id_ciclo, String codigo, String estado) {
        this.id_practicante = id_practicante;
        this.id_escuela_profesional = id_escuela_profesional;
        this.id_ciclo = id_ciclo;
        this.codigo = codigo;
        this.estado = estado;
    }

    public Practicante() {
        
    }

    public int getId_practicante() {
        return id_practicante;
    }

    public void setId_practicante(int id_practicante) {
        this.id_practicante = id_practicante;
    }

    public int getId_escuela_profesional() {
        return id_escuela_profesional;
    }

    public void setId_escuela_profesional(int id_escuela_profesional) {
        this.id_escuela_profesional = id_escuela_profesional;
    }

    public int getId_ciclo() {
        return id_ciclo;
    }

    public void setId_ciclo(int id_ciclo) {
        this.id_ciclo = id_ciclo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    } 
    
}
