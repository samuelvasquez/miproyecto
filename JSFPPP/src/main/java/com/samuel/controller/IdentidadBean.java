/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.DocIdentidadDao;
import com.samuel.daoimpl.DocIdentidadDaoImpl;
import com.samuel.model.DocIdentidad;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


@ManagedBean(name = "identidadBean")
@RequestScoped
public class IdentidadBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private DocIdentidad model;
    private DocIdentidadDao dao;
    List<DocIdentidad> list = new ArrayList<DocIdentidad>();
    String button;

    public IdentidadBean() {
        this.model = new DocIdentidad();
        this.dao = new DocIdentidadDaoImpl();
        //list = dao.listarDocIdentidad();
    }

    public void registrarDocIdentidad() {
        System.out.println("kkkkkkkk");
        this.dao = new DocIdentidadDaoImpl();
        System.out.println("id:" + model.getId_doc_identidad());
        System.out.println("'Inserting...");
        if (model.getId_doc_identidad() == 0) {
            boolean res = dao.crearDocIdentidad(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new DocIdentidad();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            boolean res = dao.actualizarDocIdentidad(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new DocIdentidad();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<DocIdentidad> getList() {
        //this.dao = new DocIdentidadDaoImpl();
        list = dao.listarDocIdentidad();

        return list;
    }

    public void prepareUpdate(DocIdentidad DI) {
        System.out.println("preparando update");
        this.model = new DocIdentidad();
        this.model = DI;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("preparando insert");
        this.model = new DocIdentidad();
        this.button = "Registrar";
    }

    public void actualizarDocIdentidad() {
        this.dao = new DocIdentidadDaoImpl();
        if (dao.actualizarDocIdentidad(model)) {
            System.out.println("Actualizado");
            this.model = new DocIdentidad();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarDocIdentidad(int id) {
        System.out.println("id:" + id);
        this.dao = new DocIdentidadDaoImpl();
        if (dao.eliminarDocIdentidad(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public DocIdentidad getModel() {
        return model;
    }

    public void setModel(DocIdentidad model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}
