/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.TipoEmpresaDao;
import com.samuel.daoimpl.TipoEmpresaDaoImpl;
import com.samuel.model.TipoEmpresa;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


@ManagedBean(name = "tipoempresaBean")
@RequestScoped
public class TipoEmpresaBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private TipoEmpresa model;
    private TipoEmpresaDao dao;
    List<TipoEmpresa> list = new ArrayList<TipoEmpresa>();
    String button;

    public TipoEmpresaBean() {
        this.model = new TipoEmpresa();
        this.dao = new TipoEmpresaDaoImpl();
    }

    public void registrarTipoEmpresa() {
        System.out.println("kkkkkkkk");
        this.dao = new TipoEmpresaDaoImpl();
        System.out.println("id:" + model.getId_tipo_empresa());
        System.out.println("'Inserting...");
        if (model.getId_tipo_empresa() == 0) {
            boolean res = dao.crearTipoEmpresa(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new TipoEmpresa();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            boolean res = dao.actualizarTipoEmpresa(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new TipoEmpresa();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<TipoEmpresa> getList() {
        list = dao.listarTipoEmpresa();

        return list;
    }

    public void prepareUpdate(TipoEmpresa DI) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new TipoEmpresa();
        this.model = DI;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new TipoEmpresa();
        this.button = "Registrar";
    }

    public void actualizarTipoEmpresa() {
        this.dao = new TipoEmpresaDaoImpl();
        if (dao.actualizarTipoEmpresa(model)) {
            System.out.println("Actualizado");
            this.model = new TipoEmpresa();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarTipoEmpresa(int id) {
        System.out.println("id:" + id);
        this.dao = new TipoEmpresaDaoImpl();
        if (dao.eliminarTipoEmpresa(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public TipoEmpresa getModel() {
        return model;
    }

    public void setModel(TipoEmpresa model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}
