/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;


import com.samuel.dao.CuestionarioDao;
import com.samuel.daoimpl.CuestionarioDaoImpl;
import com.samuel.model.Cuestionario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Shamu
 */
@ManagedBean(name = "cuestionarioBean")
@RequestScoped
public class CuestionarioBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Cuestionario model;
    private CuestionarioDao dao;
    List<Cuestionario> list = new ArrayList<Cuestionario>();
    String button;

    public CuestionarioBean() {
        this.model = new Cuestionario();
        this.dao = new CuestionarioDaoImpl();
    }

    public void registrarCuestionario() {
        System.out.println("kkkkkkkk");
        this.dao = new CuestionarioDaoImpl();
        System.out.println("id:" + model.getId_cuestionario());
        
        if (model.getId_cuestionario() == 0) {
            System.out.println("'Inserting...");
            boolean res = dao.crearCuestionario(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new Cuestionario();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            System.out.println("'Updating...");
            boolean res = dao.actualizarCuestionario(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new Cuestionario();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<Cuestionario> getList() {
        list = dao.listarCuestionario();

        return list;
    }

    public void prepareUpdate(Cuestionario cus) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Cuestionario();
        this.model = cus;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Cuestionario();
        this.button = "Registrar";
    }

    public void actualizarCuestionario() {
        this.dao = new CuestionarioDaoImpl();
        if (dao.actualizarCuestionario(model)) {
            System.out.println("Actualizado");
            this.model = new Cuestionario();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarCuestionario(int id) {
        System.out.println("id:" + id);
        this.dao = new CuestionarioDaoImpl();
        if (dao.eliminarCuestionario(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Cuestionario getModel() {
        return model;
    }

    public void setModel(Cuestionario model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}
