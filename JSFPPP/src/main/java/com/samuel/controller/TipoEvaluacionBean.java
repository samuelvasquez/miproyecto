/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.TipoEvaluacionDao;
import com.samuel.daoimpl.TipoEvaluacionDaoImpl;
import com.samuel.model.TipoEvaluacion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Shamu
 */
@ManagedBean(name = "tipoevaluacionBean")
@RequestScoped
public class TipoEvaluacionBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private TipoEvaluacion model;
    private TipoEvaluacionDao dao;
    List<TipoEvaluacion> list = new ArrayList<TipoEvaluacion>();
    String button;

    public TipoEvaluacionBean() {
        this.model = new TipoEvaluacion();
        this.dao = new TipoEvaluacionDaoImpl();
        //list = dao.listarDocIdentidad();
    }

    public void registrarTipoEvaluacion() {
        System.out.println("kkkkkkkk");
        this.dao = new TipoEvaluacionDaoImpl();
        System.out.println("id:" + model.getId_tipo());
        System.out.println("'Inserting...");
        if (model.getId_tipo() == 0) {
            boolean res = dao.crearTipoEvaluacion(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new TipoEvaluacion();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            boolean res = dao.actualizarTipoEvaluacion(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new TipoEvaluacion();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<TipoEvaluacion> getList() {
        //this.dao = new DocIdentidadDaoImpl();
        list = dao.listarTipoEvaluacion();

        return list;
    }

    public void prepareUpdate(TipoEvaluacion tev) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new TipoEvaluacion();
        this.model = tev;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new TipoEvaluacion();
        this.button = "Registrar";
    }

    public void actualizarTipoEvaluacion() {
        this.dao = new TipoEvaluacionDaoImpl();
        if (dao.actualizarTipoEvaluacion(model)) {
            System.out.println("Actualizado");
            this.model = new TipoEvaluacion();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarTipoEvaluacion(int id) {
        System.out.println("id:" + id);
        this.dao = new TipoEvaluacionDaoImpl();
        if (dao.eliminarTipoEvaluacion(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public TipoEvaluacion getModel() {
        return model;
    }

    public void setModel(TipoEvaluacion model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}

