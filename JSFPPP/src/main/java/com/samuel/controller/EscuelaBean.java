/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.EscuelaDao;
import com.samuel.daoimpl.EscuelaDaoImpl;
import com.samuel.model.Escuela;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


@ManagedBean(name = "escuelaBean")
@RequestScoped
public class EscuelaBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Escuela model;
    private EscuelaDao dao;
    List<Escuela> list = new ArrayList<Escuela>();
    String button;

    public EscuelaBean() {
        this.model = new Escuela();
        this.dao = new EscuelaDaoImpl();
        //list = dao.listarDocIdentidad();
    }

    public void registrarEscuela() {
        System.out.println("kkkkkkkk");
        this.dao = new EscuelaDaoImpl();
        System.out.println("id:" + model.getId_facultad());
        
        if (model.getId_escuela() == 0) {
            System.out.println("'Inserting...");
            boolean res = dao.crearEscuela(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new Escuela();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            System.out.println("'Updating...");
            boolean res = dao.actualizarEscuela(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new Escuela();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<Escuela> getList() {
        //this.dao = new DocIdentidadDaoImpl();
        list = dao.listarEscuela();
        return list;
    }

    public void prepareUpdate(Escuela es) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Escuela();
        this.model = es;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Escuela();
        this.button = "Registrar";
    }

    public void actualizarEscuela() {
        this.dao = new EscuelaDaoImpl();
        if (dao.actualizarEscuela(model)) {
            System.out.println("Actualizado");
            this.model = new Escuela();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarEscuela(int id) {
        System.out.println("id:" + id);
        this.dao = new EscuelaDaoImpl();
        if (dao.eliminarEscuela(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Escuela getModel() {
        return model;
    }

    public void setModel(Escuela model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}
