/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.TipoPreguntaDao;
import com.samuel.daoimpl.TipoPreguntaDaoImpl;
import com.samuel.model.TipoPregunta;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Shamu
 */
@ManagedBean(name = "tipopreguntaBean")
@RequestScoped
public class TipoPreguntaBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private TipoPregunta model;
    private TipoPreguntaDao dao;
    List<TipoPregunta> list = new ArrayList<TipoPregunta>();
    String button;

    public TipoPreguntaBean() {
        this.model = new TipoPregunta();
        this.dao = new TipoPreguntaDaoImpl();
        //list = dao.listarDocIdentidad();
    }

    public void registrarTipoPregunta() {
        System.out.println("kkkkkkkk");
        this.dao = new TipoPreguntaDaoImpl();
        System.out.println("id:" + model.getId_tipo());
        System.out.println("'Inserting...");
        if (model.getId_tipo() == 0) {
            boolean res = dao.crearTipoPregunta(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new TipoPregunta();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            boolean res = dao.actualizarTipoPregunta(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new TipoPregunta();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<TipoPregunta> getList() {
        //this.dao = new DocIdentidadDaoImpl();
        list = dao.listarTipoPregunta();

        return list;
    }

    public void prepareUpdate(TipoPregunta tipre) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new TipoPregunta();
        this.model = tipre;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new TipoPregunta();
        this.button = "Registrar";
    }

    public void actualizarTipoPregunta() {
        this.dao = new TipoPreguntaDaoImpl();
        if (dao.actualizarTipoPregunta(model)) {
            System.out.println("Actualizado");
            this.model = new TipoPregunta();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarTipoPregunta(int id) {
        System.out.println("id:" + id);
        this.dao = new TipoPreguntaDaoImpl();
        if (dao.eliminarTipoPregunta(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public TipoPregunta getModel() {
        return model;
    }

    public void setModel(TipoPregunta model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}
