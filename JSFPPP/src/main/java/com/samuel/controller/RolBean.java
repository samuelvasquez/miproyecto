/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;


import com.samuel.dao.RolDao;
import com.samuel.daoimpl.RolDaoImpl;
import com.samuel.model.Rol;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Shamu
 */
@ManagedBean(name = "rolBean")
@RequestScoped
public class RolBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Rol model;
    private RolDao dao;
    List<Rol> list = new ArrayList<Rol>();
    String button;

    public RolBean() {
        this.model = new Rol();
        this.dao = new RolDaoImpl();
    }

    public void registrarEmpresa() {
        System.out.println("kkkkkkkk");
        this.dao = new RolDaoImpl();
        System.out.println("id:" + model.getId_rol());
        
        if (model.getId_rol() == 0) {
            System.out.println("'Inserting...");
            boolean res = dao.crearRol(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new Rol();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            System.out.println("'Updating...");
            boolean res = dao.actualizarRol(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new Rol();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<Rol> getList() {
        list = dao.listarRol();

        return list;
    }

    public void prepareUpdate(Rol rol) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Rol();
        this.model = rol;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Rol();
        this.button = "Registrar";
    }

    public void actualizarRol() {
        this.dao = new RolDaoImpl();
        if (dao.actualizarRol(model)) {
            System.out.println("Actualizado");
            this.model = new Rol();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarRol(int id) {
        System.out.println("id:" + id);
        this.dao = new RolDaoImpl();
        if (dao.eliminarRol(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Rol getModel() {
        return model;
    }

    public void setModel(Rol model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}
