/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.PracticaDao;
import com.samuel.daoimpl.PracticaDaoImpl;
import com.samuel.model.Practica;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Shamu
 */
@ManagedBean(name = "practicaBean")
@RequestScoped
public class PracticaBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Practica model;
    private PracticaDao dao;
    List<Practica> list = new ArrayList<Practica>();
    String button;

    public PracticaBean() {
        this.model = new Practica();
        this.dao = new PracticaDaoImpl();
    }

    public void registrarPractica() {
            this.dao = new PracticaDaoImpl();
            System.out.println("id:" + model.getId_practica());
        if (model.getId_practica()== 0) {
            System.out.println("'Inserting...");
            boolean res = dao.crearPractica(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new Practica();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            System.out.println("'Updating...");
            boolean res = dao.actualizarPractica(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new Practica();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<Practica> getList() {
        list = dao.listarPractica();

        return list;
    }

    public void prepareUpdate(Practica pr) {
         System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Practica();
        this.model = pr;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Practica();
    }

    public void actualizarPractica() {
        this.dao = new PracticaDaoImpl();
        if (dao.actualizarPractica(model)) {
            System.out.println("Actualizado");
            this.model = new Practica();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }
    public void eliminarPractica(int id) {
        System.out.println("id:" + id);
        this.dao = new PracticaDaoImpl();
        if (dao.eliminarPractica(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Practica getModel() {
        return model;
    }

    public void setModel(Practica model) {
        this.model = model;
    }
    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }
    
    

}

