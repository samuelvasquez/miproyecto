/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.EmpresaDao;
import com.samuel.daoimpl.EmpresaDaoImpl;
import com.samuel.model.Empresa;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


@ManagedBean(name = "empresaBean")
@RequestScoped
public class EmpresaBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Empresa model;
    private EmpresaDao dao;
    List<Empresa> list = new ArrayList<Empresa>();
    String button;

    public EmpresaBean() {
        this.model = new Empresa();
        this.dao = new EmpresaDaoImpl();
    }

    public void registrarEmpresa() {
        System.out.println("kkkkkkkk");
        this.dao = new EmpresaDaoImpl();
        System.out.println("id:" + model.getId_empresa());
        
        if (model.getId_empresa() == 0) {
            System.out.println("'Inserting...");
            boolean res = dao.crearEmpresa(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new Empresa();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            System.out.println("'Updating...");
            boolean res = dao.actualizarEmpresa(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new Empresa();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<Empresa> getList() {
        list = dao.listarEmpresa();

        return list;
    }

    public void prepareUpdate(Empresa e) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Empresa();
        this.model = e;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Empresa();
        this.button = "Registrar";
    }

    public void actualizarEmpresa() {
        this.dao = new EmpresaDaoImpl();
        if (dao.actualizarEmpresa(model)) {
            System.out.println("Actualizado");
            this.model = new Empresa();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarEmpresa(int id) {
        System.out.println("id:" + id);
        this.dao = new EmpresaDaoImpl();
        if (dao.eliminarEmpresa(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Empresa getModel() {
        return model;
    }

    public void setModel(Empresa model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}
