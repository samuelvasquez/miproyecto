/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.FacultadDao;
import com.samuel.daoimpl.FacultadDaoImpl;
import com.samuel.model.Facultad;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


@ManagedBean(name = "facultadBean")
@RequestScoped
public class FacultadBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Facultad model;
    private FacultadDao dao;
    List<Facultad> list = new ArrayList<Facultad>();
    String button;

    public FacultadBean() {
        this.model = new Facultad();
        this.dao = new FacultadDaoImpl();
        //list = dao.listarDocIdentidad();
    }

    public void registrarFacultad() {
        System.out.println("kkkkkkkk");
        this.dao = new FacultadDaoImpl();
        System.out.println("id:" + model.getId_facultad());
        System.out.println("'Inserting...");
        if (model.getId_facultad() == 0) {
            boolean res = dao.crearFacultad(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new Facultad();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            boolean res = dao.actualizarFacultad(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new Facultad();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<Facultad> getList() {
        //this.dao = new DocIdentidadDaoImpl();
        list = dao.listarFacultad();

        return list;
    }

    public void prepareUpdate(Facultad fa) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Facultad();
        this.model = fa;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Facultad();
        this.button = "Registrar";
    }

    public void actualizarFacultad() {
        this.dao = new FacultadDaoImpl();
        if (dao.actualizarFacultad(model)) {
            System.out.println("Actualizado");
            this.model = new Facultad();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarFacultad(int id) {
        System.out.println("id:" + id);
        this.dao = new FacultadDaoImpl();
        if (dao.eliminarFacultad(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Facultad getModel() {
        return model;
    }

    public void setModel(Facultad model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}
