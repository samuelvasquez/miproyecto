/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.PreguntaDao;
import com.samuel.daoimpl.PreguntaDaoImpl;
import com.samuel.model.Pregunta;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Shamu
 */
@ManagedBean(name = "preguntaBean")
@RequestScoped
public class PreguntaBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Pregunta model;
    private PreguntaDao dao;
    List<Pregunta> list = new ArrayList<Pregunta>();
    String button;

    public PreguntaBean() {
        this.model = new Pregunta();
        this.dao = new PreguntaDaoImpl();
    }

    public void registrarPregunta() {
        System.out.println("kkkkkkkk");
        this.dao = new PreguntaDaoImpl();
        System.out.println("id:" + model.getId_pregunta());
        System.out.println("'Inserting...");
        if (model.getId_pregunta() == 0) {
            boolean res = dao.crearPregunta(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new Pregunta();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            boolean res = dao.actualizarPregunta(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new Pregunta();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<Pregunta> getList() {
        list = dao.listarPregunta();

        return list;
    }

    public void prepareUpdate(Pregunta DI) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Pregunta();
        this.model = DI;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Pregunta();
        this.button = "Registrar";
    }

    public void actualizarPregunta() {
        this.dao = new PreguntaDaoImpl();
        if (dao.actualizarPregunta(model)) {
            System.out.println("Actualizado");
            this.model = new Pregunta();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarPregunta(int id) {
        System.out.println("id:" + id);
        this.dao = new PreguntaDaoImpl();
        if (dao.eliminarPregunta(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Pregunta getModel() {
        return model;
    }

    public void setModel(Pregunta model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}

