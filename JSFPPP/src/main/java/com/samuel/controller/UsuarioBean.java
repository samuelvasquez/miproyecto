/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.UsuarioDao;
import com.samuel.daoimpl.UsuarioDaoImpl;
import com.samuel.model.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Shamu
 */
@ManagedBean(name = "usuarioBean")
@RequestScoped
public class UsuarioBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Usuario model;
    private UsuarioDao dao;
    List<Usuario> list = new ArrayList<Usuario>();
    String button;

    public UsuarioBean() {
        this.model = new Usuario();
        this.dao = new UsuarioDaoImpl();
    }

    public void registrarUsuario() {
        System.out.println("kkkkkkkk");
        this.dao = new UsuarioDaoImpl();
        System.out.println("id:" + model.getId_usuario());
        
        if (model.getId_usuario() == 0) {
            System.out.println("'Inserting...");
            boolean res = dao.crearUsuario(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new Usuario();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            System.out.println("'Updating...");
            boolean res = dao.actualizarUsuario(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new Usuario();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<Usuario> getList() {
        list = dao.listarUsuario();

        return list;
    }

    public void prepareUpdate(Usuario us) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Usuario();
        this.model = us;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Usuario();
        this.button = "Registrar";
    }

    public void actualizarUsuario() {
        this.dao = new UsuarioDaoImpl();
        if (dao.actualizarUsuario(model)) {
            System.out.println("Actualizado");
            this.model = new Usuario();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarUsuario(int id) {
        System.out.println("id:" + id);
        this.dao = new UsuarioDaoImpl();
        if (dao.eliminarUsuario(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Usuario getModel() {
        return model;
    }

    public void setModel(Usuario model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}
