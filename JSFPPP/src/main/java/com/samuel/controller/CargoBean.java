/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.CargoDao;
import com.samuel.daoimpl.CargoDaoImpl;
import com.samuel.model.Cargo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean(name = "cargoBean")
@RequestScoped
public class CargoBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Cargo model;
    private CargoDao dao;
    List<Cargo> list = new ArrayList<Cargo>();
    String button;

    public CargoBean() {
        this.model = new Cargo();
        this.dao = new CargoDaoImpl();
    }

    public void registrarCargo() {
        System.out.println("kkkkkkkk");
        this.dao = new CargoDaoImpl();
        System.out.println("id:" + model.getId_cargo());
        
        if (model.getId_cargo() == 0) {
            System.out.println("'Inserting...");
            boolean res = dao.crearCargo(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new Cargo();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            System.out.println("'Updating...");
            boolean res = dao.actualizarCargo(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new Cargo();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<Cargo> getList() {
        list = dao.listarCargo();

        return list;
    }

    public void prepareUpdate(Cargo ca) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Cargo();
        this.model = ca;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Cargo();
        this.button = "Registrar";
    }

    public void actualizarCargo() {
        this.dao = new CargoDaoImpl();
        if (dao.actualizarCargo(model)) {
            System.out.println("Actualizado");
            this.model = new Cargo();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarCargo(int id) {
        System.out.println("id:" + id);
        this.dao = new CargoDaoImpl();
        if (dao.eliminarCargo(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Cargo getModel() {
        return model;
    }

    public void setModel(Cargo model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}
