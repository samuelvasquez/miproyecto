/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.CicloDao;
import com.samuel.daoimpl.CicloDaoImpl;
import com.samuel.model.Ciclo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


@ManagedBean(name = "cicloBean")
@RequestScoped
public class CicloBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Ciclo model;
    private CicloDao dao;
    List<Ciclo> list = new ArrayList<Ciclo>();
    String button;

    public CicloBean() {
        this.model = new Ciclo();
        this.dao = new CicloDaoImpl();
    }

    public void registrarCiclo() {
        System.out.println("kkkkkkkk");
        this.dao = new CicloDaoImpl();
        System.out.println("id:" + model.getId_ciclo());
        System.out.println("'Inserting...");
        if (model.getId_ciclo() == 0) {
            boolean res = dao.crearCiclo(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new Ciclo();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            boolean res = dao.actualizarCiclo(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new Ciclo();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<Ciclo> getList() {
        //this.dao = new DocIdentidadDaoImpl();
        System.out.println("Ingresamos a la lista");
        list = dao.listarCiclo();
        System.out.println("lista");
        System.out.println(list);

        return list;
    }

    public void prepareUpdate(Ciclo ci) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Ciclo();
        this.model = ci;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Ciclo();
        this.button = "Registrar";
    }

    public void actualizarCiclo() {
        this.dao = new CicloDaoImpl();
        if (dao.actualizarCiclo(model)) {
            System.out.println("Actualizado");
            this.model = new Ciclo();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarCiclo(int id) {
        System.out.println("id:" + id);
        this.dao = new CicloDaoImpl();
        if (dao.eliminarCiclo(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Ciclo getModel() {
        return model;
    }

    public void setModel(Ciclo model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}
