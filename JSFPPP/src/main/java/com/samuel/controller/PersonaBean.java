/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.PersonaDao;
import com.samuel.daoimpl.PersonaDaoImpl;
import com.samuel.model.Persona;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


@ManagedBean(name = "personaBean")
@RequestScoped
public class PersonaBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Persona model;
    private PersonaDao dao;
    private List<Persona> list = new ArrayList<Persona>();
    String button;

    public PersonaBean() {
        this.model = new Persona();
        this.dao = new PersonaDaoImpl();
        //this.tipo_doc_identidad = listar_tipo_documento();
    }
    
    public void registrarPersona() {
        this.dao = new PersonaDaoImpl();
        if (model.getId_persona() == 0) {
            System.out.println("'Inserting...");
            boolean res = dao.crearPersona(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new Persona();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            System.out.println("'Updating...");
            boolean res = dao.actualizarPersona(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new Persona();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<Persona> getList() {
        list = this.dao.listarPersona();
        
        return list;
    }

    public void prepareUpdate(Persona per) {
        this.model = new Persona();
        this.model = per; 
        this.button = "Actualizar";
        
    }
    
    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Persona();
        this.button = "Registrar";
    }

    public void actualizarPersona() {
        this.dao = new PersonaDaoImpl();
        if (dao.actualizarPersona(model)) {
            System.out.println("Actualizado");
            this.model = new Persona();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarPersona(int id) {
        System.out.println("id:" + id);
        this.dao = new PersonaDaoImpl();
        if (dao.eliminarPersona(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Persona getModel() {
        return model;
    }

    public void setModel(Persona model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

    
}
