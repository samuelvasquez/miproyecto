/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.AlternativaDao;
import com.samuel.daoimpl.AlternativaDaoImpl;
import com.samuel.model.Alternativa;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Shamu
 */
@ManagedBean(name = "alternativaBean")
@RequestScoped
public class AlternativaBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Alternativa model;
    private AlternativaDao dao;
    List<Alternativa> list = new ArrayList<Alternativa>();
    String button;

    public AlternativaBean() {
        this.model = new Alternativa();
        this.dao = new AlternativaDaoImpl();
    }

    public void registrarAlternativa() {
        System.out.println("kkkkkkkk");
        this.dao = new AlternativaDaoImpl();
        System.out.println("id:" + model.getId_alternativa());
        
        if (model.getId_alternativa() == 0) {
            System.out.println("'Inserting...");
            boolean res = dao.crearAlternativa(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new Alternativa();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            System.out.println("'Updating...");
            boolean res = dao.actualizarAlternativa(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new Alternativa();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<Alternativa> getList() {
        list = dao.listarAlternativa();

        return list;
    }

    public void prepareUpdate(Alternativa al) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Alternativa();
        this.model = al;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Alternativa();
        this.button = "Registrar";
    }

    public void actualizarAlternativa() {
        this.dao = new AlternativaDaoImpl();
        if (dao.actualizarAlternativa(model)) {
            System.out.println("Actualizado");
            this.model = new Alternativa();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarAlternativa(int id) {
        System.out.println("id:" + id);
        this.dao = new AlternativaDaoImpl();
        if (dao.eliminarAlternativa(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Alternativa getModel() {
        return model;
    }

    public void setModel(Alternativa model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}
