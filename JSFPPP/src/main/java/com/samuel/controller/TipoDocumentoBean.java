/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.TipoDocumentoDao;
import com.samuel.daoimpl.TipoDocumentoDaoImpl;
import com.samuel.model.TipoDocumento;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


@ManagedBean(name = "tipodocumentoBean")
@RequestScoped
public class TipoDocumentoBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private TipoDocumento model;
    private TipoDocumentoDao dao;
    List<TipoDocumento> list = new ArrayList<TipoDocumento>();
    String button;

    public TipoDocumentoBean() {
        this.model = new TipoDocumento();
        this.dao = new TipoDocumentoDaoImpl();
    }

    public void registrarTipoDocumento() {
        System.out.println("kkkkkkkk");
        this.dao = new TipoDocumentoDaoImpl();
        System.out.println("id:" + model.getId_tipo_documento());
        System.out.println("'Inserting...");
        if (model.getId_tipo_documento() == 0) {
            boolean res = dao.crearTipoDocumento(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new TipoDocumento();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            boolean res = dao.actualizarTipoDocumento(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new TipoDocumento();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<TipoDocumento> getList() {
        list = dao.listarTipoDocumento();

        return list;
    }

    public void prepareUpdate(TipoDocumento DI) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new TipoDocumento();
        this.model = DI;
        this.button = "Actualizar";
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new TipoDocumento();
        this.button = "Registrar";
    }

    public void actualizarTipoDocumento() {
        this.dao = new TipoDocumentoDaoImpl();
        if (dao.actualizarTipoDocumento(model)) {
            System.out.println("Actualizado");
            this.model = new TipoDocumento();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarTipoDocumento(int id) {
        System.out.println("id:" + id);
        this.dao = new TipoDocumentoDaoImpl();
        if (dao.eliminarTipoDocumento(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public TipoDocumento getModel() {
        return model;
    }

    public void setModel(TipoDocumento model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

}
