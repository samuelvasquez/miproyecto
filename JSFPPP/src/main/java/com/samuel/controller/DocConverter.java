/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.model.DocIdentidad;
import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;


@FacesConverter(value = "docConverter")
public class DocConverter implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String docId) {
        System.out.println("getAsObject:" + docId);
 
        ValueExpression vex = fc.getApplication().getExpressionFactory().createValueExpression(fc.getELContext(),
                "com.samuel.controller.IdentidadBean@4a78363a", IdentidadBean.class);
 
        IdentidadBean beers = (IdentidadBean) vex.getValue(fc.getELContext());
        return beers.getModel();
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        return String.valueOf(((DocIdentidad) o).getId_doc_identidad());
    }
 
    
}
