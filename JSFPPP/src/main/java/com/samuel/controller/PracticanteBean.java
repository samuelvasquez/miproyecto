/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.PracticanteDao;
import com.samuel.daoimpl.PracticanteDaoImpl;
import com.samuel.model.Practicante;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;


@ManagedBean(name = "practicanteBean")
@RequestScoped
public class PracticanteBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Practicante model;
    private PracticanteDao dao;
    List<Practicante> list = new ArrayList<Practicante>();
    

    public PracticanteBean() {
        this.model = new Practicante();
        this.dao = new PracticanteDaoImpl();
    }

    public void registrarPracticante() {
        System.out.println("kkkkkkkk");
        this.dao = new PracticanteDaoImpl();
        System.out.println("id:" + model.getId_practicante());

        System.out.println("'Inserting...");
        boolean res = dao.crearPracticante(model);
        if (res) {
            System.out.println("Insertado");
            this.model = new Practicante();
        } else {
            System.out.println("Error al momento de insertar");
        }

    }

    public List<Practicante> getList() {
        list = dao.listarPracticante();

        return list;
    }

    public void prepareUpdate(Practicante pr) {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Practicante();
        System.out.println("codigo:"+pr.getCodigo());
        this.model = pr;
    }

    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Practicante();
    }

    public void actualizarPracticante() {
        this.dao = new PracticanteDaoImpl();
        if (dao.actualizarPracticante(model)) {
            System.out.println("Actualizado");
            this.model = new Practicante();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }
    public void eliminarPracticante(int id) {
        System.out.println("id:" + id);
        this.dao = new PracticanteDaoImpl();
        if (dao.eliminarPracticante(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Practicante getModel() {
        return model;
    }

    public void setModel(Practicante model) {
        this.model = model;
    }



}
