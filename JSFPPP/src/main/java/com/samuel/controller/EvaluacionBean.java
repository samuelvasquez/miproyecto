/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.samuel.controller;

import com.samuel.dao.EvaluacionDao;
import com.samuel.daoimpl.EvaluacionDaoImpl;
import com.samuel.model.Evaluacion;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

/**
 *
 * @author Shamu
 */
@ManagedBean(name = "evaluacionBean")
@RequestScoped
public class EvaluacionBean implements Serializable {

    private static final long serialVersionUID = 1L;
    private Evaluacion model;
    private EvaluacionDao dao;
    private List<Evaluacion> list = new ArrayList<Evaluacion>();
    String button;

    public EvaluacionBean() {
        this.model = new Evaluacion();
        this.dao = new EvaluacionDaoImpl();
        //this.tipo_doc_identidad = listar_tipo_documento();
    }
    
    public void registrarEvaluacion() {
        this.dao = new EvaluacionDaoImpl();
        if (model.getId_evaluacion() == 0) {
            System.out.println("'Inserting...");
            boolean res = dao.crearEvaluacion(model);
            if (res) {
                System.out.println("Insertado");
                this.model = new Evaluacion();
            } else {
                System.out.println("Error al momento de insertar");
            }
        } else {
            System.out.println("'Updating...");
            boolean res = dao.actualizarEvaluacion(model);
            if (res) {
                System.out.println("Actualizado");
                this.model = new Evaluacion();
            } else {
                System.out.println("Error al momento de actualizar");
            }
        }
    }

    public List<Evaluacion> getList() {
        list = this.dao.listarEvaluacion();
        
        return list;
    }

    public void prepareUpdate(Evaluacion ev) {
        this.model = new Evaluacion();
        this.model = ev; 
        this.button = "Actualizar";
        
    }
    
    public void prepareInsert() {
        System.out.println("jiijijijiijjjjjjjjj");
        this.model = new Evaluacion();
        this.button = "Registrar";
    }

    public void actualizarEvaluacion() {
        this.dao = new EvaluacionDaoImpl();
        if (dao.actualizarEvaluacion(model)) {
            System.out.println("Actualizado");
            this.model = new Evaluacion();
        } else {
            System.out.println("Error al momento de actualizar");
        }
    }

    public void eliminarEvaluacion(int id) {
        System.out.println("id:" + id);
        this.dao = new EvaluacionDaoImpl();
        if (dao.eliminarEvaluacion(id)) {
            System.out.println("Eliminado:" + id);
        } else {
            System.out.println("Error al momento de eliminar");
        }
    }

    public Evaluacion getModel() {
        return model;
    }

    public void setModel(Evaluacion model) {
        this.model = model;
    }

    public String getButton() {
        return button;
    }

    public void setButton(String button) {
        this.button = button;
    }

    
}
